# Mobile End of Term Project

Created by Nicola Castellani (***VR361872***) & Nicola Cisternino (***VR466072***).

## Introduction

***AutoBattler!*** is a single player auto battler game where the player has to select units to face a randomly placed troop from an IA.

The player has a 4x5 grid to place his units, and the IA too.

In each level the player needs to elaborate different solutions to face different kind of units:

1. **Melee**
2. **Archers**
3. **Priests**
4. **Heroes**
5. **Towers**

The player wins under these conditions:

1. The enemy army is **defeated** (no one alive)
2. The time ends and the player **has more units than the IA**

The player loses under these conditions:

1. The player **loses** when all his units dies
2. The time ends and the player has a number of units **less or equal** than the IA

---
### Game modes  

The player can play with a ***Story mode***, which contains 5 different "tutorial" levels, and an ***Arena Mode*** to play infinite battles with no strict limits.

---
## Project structure

* ***Artwork***  
  Contains all the sprites, sounds, fonts and other creative assets of the project.  
* ***Externals***  
  Contains external packages.
* ***Plugins***  
  Contains Unity plugins.
* ***Resources***  
  Contains Unity resources
* ***Scenes***  
  Contains game scenes
* ***Technical***  
  Contains scripts and tests.

---

## How to play

You can start a new game from the **MainMenu** scene, here you have 3 options:

1. ***Story Mode***   
   *Start a new adventure!*
2. ***Arena Mode***  
   *Battle against random troops!*
3. ***Settings***   
   *Manage some of the game settings, and reset user data.*
   
Starting a new game will prompt a simple ***tutorial*** to the user, to let him know how to play during the game.

## Camera

To let users enjoy a better experience the Camera was developed with movement in mind by 
executing some fixed gestures. 
The available actions of the camera are the following:

* **Zoomed IN/OUT** : by pinching in or out with two fingers
* **Rotated** : by sliding one finger from left to right or vice versa