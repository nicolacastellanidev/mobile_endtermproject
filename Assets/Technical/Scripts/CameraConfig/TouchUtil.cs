﻿using UnityEngine;

namespace Technical.Scripts.CameraConfig
{
    public static class TouchUtil
    {
        /// <summary>
        /// Minimum distance needed in order to detect a finger movement
        /// </summary>
        public static float minMovementDistance = 1f;
        /// <summary>
        /// Minimum distance needed in order to detect a pinch action
        /// </summary>
        public static float minPinchDistance = 1f;
        /// <summary>
        /// Minimum angle needed in order to detect a rotation action
        /// </summary>
        public static float minAngle = 0f;

        public static Vector3 GetTouchMovementDirection(Touch touch)
        {
            Vector3 movementDisplacement = Vector3.zero;
            //use negated values in order to have inverse axis
            movementDisplacement.x = -touch.deltaPosition.x;
            movementDisplacement.z = -touch.deltaPosition.y;

            float sqrMinDistance = minMovementDistance * minMovementDistance;
            
            return movementDisplacement.sqrMagnitude >= sqrMinDistance
                ? movementDisplacement
                : Vector3.zero;
        }
        
        public static float GetTouchPinch(Touch firstTouch, Touch secondTouch)
        {
            var touch1Previous = firstTouch.position - firstTouch.deltaPosition;
            var touch2Previous = secondTouch.position - secondTouch.deltaPosition;

            var oldTouchDistance = Vector2.Distance(touch1Previous, touch2Previous);
            var currentTouchDistance = Vector2.Distance(firstTouch.position, secondTouch.position);
                
            float pinchDistance = (oldTouchDistance - currentTouchDistance);

            return Mathf.Abs(pinchDistance) > minPinchDistance ? pinchDistance : 0f;
        }
        public static float GetTouchRotation(Touch firstTouch, Touch secondTouch)
        {
            //negative means the touch is on the left part, right otherwise
            float xScreen = firstTouch.position.x - secondTouch.position.x;
            //negative means the touch is on the bottom part, top otherwise
            float yScreen = firstTouch.position.y - secondTouch.position.y;

            //-1 means clockwise
            //+1 means anticlockwise
            float rotationValue = (yScreen * xScreen) > 0 ? -1 : 1; //normalize values
            float angle = Mathf.DeltaAngle(firstTouch.position.y, secondTouch.position.y);
            
            return Mathf.Abs(angle) > minAngle ? rotationValue : 0f;
        }
        
        public static bool IsStandingStill(Touch touch)
        {
            return touch.deltaPosition.sqrMagnitude < (minMovementDistance * minMovementDistance);
        }

        public static bool IsMoving(Touch touch)
        {
            return touch.deltaPosition.sqrMagnitude >= (minMovementDistance * minMovementDistance);
        }
    }
}