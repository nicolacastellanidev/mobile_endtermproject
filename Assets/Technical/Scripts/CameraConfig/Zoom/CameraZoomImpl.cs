﻿
namespace Technical.Scripts.CameraConfig.Zoom
{
    public enum Zoom
    {
        In,
        Out,
        None
    }
    
    public static class ZoomExtension {
        public static int GetZoomDirectionValue(this Zoom zoom)
        {
            switch (zoom)
            {
                case Zoom.In: return 1;
                case Zoom.Out: return -1;
                default: return 0;
            }
        }
    }
    
    public abstract class CameraZoomImpl
    {
        public abstract Zoom GetZoomDirection();
        
        public static CameraZoomImpl Create(CameraPlatform platform)
        {
            switch (platform)
            {
                case CameraPlatform.Mobile: return new MobileCameraZoomImpl();
                default: return new PcCameraZoomImpl();
            }
        }
    }
}