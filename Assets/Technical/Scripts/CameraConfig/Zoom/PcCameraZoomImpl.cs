﻿
using UnityEngine;

namespace Technical.Scripts.CameraConfig.Zoom
{
    public class PcCameraZoomImpl : CameraZoomImpl
    {
        public override Zoom GetZoomDirection()
        {
            var value = Input.GetAxis("Mouse ScrollWheel");
            if (value == 0) return Zoom.None;
            
            return value < 0
                ? Zoom.Out
                : Zoom.In;
        }
    }
}