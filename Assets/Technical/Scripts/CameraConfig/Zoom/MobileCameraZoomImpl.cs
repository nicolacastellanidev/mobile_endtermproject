﻿using UnityEngine;

namespace Technical.Scripts.CameraConfig.Zoom
{
    public class MobileCameraZoomImpl : CameraZoomImpl 
    {
        public override Zoom GetZoomDirection()
        {
            if(Input.touchCount != 2) return Zoom.None;
            
            var touch1 = Input.GetTouch(0);
            var touch2 = Input.GetTouch(1);

            if (TouchUtil.IsMoving(touch1) && TouchUtil.IsMoving(touch2))
            {

                float zoomValue = TouchUtil.GetTouchPinch(touch1, touch2);

                if (zoomValue < 0) return Zoom.In; //zooming in
                if (zoomValue > 0) return Zoom.Out; //zooming out
            }

            return Zoom.None;
        }
    }
}