﻿using Technical.Scripts.Managers;
using UnityEngine;

namespace Technical.Scripts.CameraConfig.Zoom
{
    public class CameraZoom : CameraComponent<CameraZoomImpl>
    {
        [Space(10), Header("Zoom settings")] 
        [SerializeField] private float minZoomPinch = 8f;
        [SerializeField] private float maxZoom = 10f;
        [SerializeField] private float minZoom = 20f;
        [SerializeField] private float zoomSpeed = 2f;

        private Camera camera;
        private Vector3 terrainPosition = Vector3.zero;
        private float minCameraHeight => terrainPosition.y + maxZoom;
        private float maxCameraHeight => terrainPosition.y + minZoom;
        
        private Vector3 force = Vector3.zero;

        #region Lifecycle

        private void Start()
        {
            TouchUtil.minPinchDistance = minZoomPinch;
            camera = Camera.main;
        }

        protected override void OnUpdate(float elapsed)
        {
            base.OnUpdate(elapsed);
            force += camera.transform.forward * (elapsed * zoomSpeed  * platformImpl.GetZoomDirection().GetZoomDirectionValue());
        }

        protected override void OnFixedUpdate(float elapsed)
        {
            base.OnFixedUpdate(elapsed);
            if (force == Vector3.zero) return;
            
            float currentCameraHeight = transform.position.y;
            float updateCameraHeight = currentCameraHeight + force.y;
            
            if (updateCameraHeight < minCameraHeight || updateCameraHeight > maxCameraHeight)
                force = CalculateRemainingForceUntilLimitPoint();
            
            transform.position += force;
            force = Vector3.zero;
        }

        protected override void OnLateUpdate()
        {
            base.OnLateUpdate();
            Vector3 fixedPosition = transform.position;
            fixedPosition.y = Mathf.Clamp(fixedPosition.y, minCameraHeight, maxCameraHeight);
            transform.position = fixedPosition;
        }
        
        #endregion

        private Vector3 CalculateRemainingForceUntilLimitPoint()
        {
            float currentCameraHeight = transform.position.y;
            float updateCameraHeight = currentCameraHeight + force.y;
            float heightDifference = 0f;

            if (updateCameraHeight < minCameraHeight)
            {
                heightDifference = Mathf.Abs(currentCameraHeight - minCameraHeight);
                float cos = Vector3.Dot(force, Vector3.up);
                return force * (heightDifference / -cos);
            }

            if (updateCameraHeight > maxCameraHeight)
            {
                heightDifference = Mathf.Abs(currentCameraHeight - maxCameraHeight);
                float cos = Vector3.Dot(force, -transform.forward);
                return force * (heightDifference / cos);
            }

            return Vector3.zero;
        }
        protected override void OnCameraComponentConfig(CameraConfig config)
        {
            SetCameraPlatformImplementation(CameraZoomImpl.Create(config._platform));
            
            var cameraSettings = DataManager.Instance.GetData().settings.cameraSettings;
            maxZoom = cameraSettings.maxCameraZoom;
            minZoom = cameraSettings.minCameraZoom;
            zoomSpeed = cameraSettings.cameraZoomSpeed;
            
            terrainPosition = config._terrain.transform.position;
        }
    }
}