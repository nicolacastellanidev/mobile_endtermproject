﻿using UnityEngine;

namespace Technical.Scripts.CameraConfig
{
    public enum CameraPlatform
    {
        Pc,
        Mobile
    }
    
    public static class CameraUtil
    {
        public static CameraPlatform GetCurrentPlatform()
        {
            #if UNITY_EDITOR
            if (UnityEditor.EditorApplication.isRemoteConnected) return CameraPlatform.Mobile;
            #endif
            if (Application.isMobilePlatform) return CameraPlatform.Mobile;
            return CameraPlatform.Pc;
        }
    }
}