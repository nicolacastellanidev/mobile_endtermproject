﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.CameraConfig
{
    public struct CameraConfig
    {
        public CameraPlatform _platform;
        public GameObject _terrain;
    }
    
    [Serializable]
    public class CameraConfigEvent : UnityEvent<CameraConfig> {}
    public class RtsCameraController : MonoBehaviour
    {
        [Header("Terrain")]
        [Tooltip("GameObject used to compare distances relatively to camera")]
        [SerializeField] private GameObject terrain;
        
        public CameraConfigEvent cameraConfigEvent;

        #region Lifecycle
        private void Awake()
        {
            if (terrain != null) return;
            Debug.LogWarning("Terrain object is missing");
            Destroy(this);
        }

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(1);
            var cameraConfigParams = new CameraConfig()
            {
                _platform = CameraUtil.GetCurrentPlatform(),
                _terrain = terrain
            };
            cameraConfigEvent?.Invoke(cameraConfigParams);
        }
        #endregion
    }
}
