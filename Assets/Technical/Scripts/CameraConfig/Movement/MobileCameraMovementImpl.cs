﻿using UnityEngine;

namespace Technical.Scripts.CameraConfig.Movement
{
    public class MobileCameraMovementImpl : CameraMovementImpl
    {
        Quaternion yAxisRotation = Quaternion.identity;
        public override Vector3 GetMovementDirection(Transform cameraTransform)
        {
            //only one touch is allowed
            if (Input.touchCount != 1) return Vector3.zero;

            var touch = Input.GetTouch(0);

            Vector3 displacement = Vector3.zero;

            if (TouchUtil.IsMoving(touch))
            {
                UpdateOffsetRotation(cameraTransform.rotation);
                displacement = yAxisRotation * TouchUtil.GetTouchMovementDirection(touch);
            }

            return displacement;
        }

        private void UpdateOffsetRotation(Quaternion cameraRotation)
        {
            yAxisRotation = cameraRotation;
            yAxisRotation.x = yAxisRotation.z = 0f;
        }
    }
}