﻿using UnityEngine;

namespace Technical.Scripts.CameraConfig.Movement
{
    public class CameraMovement : CameraComponent<CameraMovementImpl>
    {
        [Space(10), Header("Movement settings")]
        [SerializeField] private float cameraMargin = 20f;
        [SerializeField] private float movementSpeed = 1f;
        [SerializeField] private float minMovementDistance = 1f;

        private Vector3 force = Vector3.zero;

        private Vector3 maxTerrainPosition = Vector3.zero;
        private Vector3 minTerrainPosition = Vector3.zero;
        
        #region LifeCycle

        private void Start()
        {
            TouchUtil.minMovementDistance = minMovementDistance;
        }

        protected override void OnUpdate(float elapsed)
        {
            base.OnUpdate(elapsed);
            Vector3 displacement = platformImpl.GetMovementDirection(transform);
            force += displacement.normalized * (movementSpeed * elapsed);
        }

        protected override void OnFixedUpdate(float elapsed)
        {
            base.OnFixedUpdate(elapsed);
            if (force == Vector3.zero) return;
            transform.position += force;
            force = Vector3.zero;
        }

        protected override void OnLateUpdate()
        {
            base.OnLateUpdate();
            Vector3 fixedPosition = transform.position;
            
            fixedPosition.x = Mathf.Clamp(fixedPosition.x, minTerrainPosition.x - cameraMargin, maxTerrainPosition.x + cameraMargin);
            fixedPosition.z = Mathf.Clamp(fixedPosition.z, minTerrainPosition.z - cameraMargin, maxTerrainPosition.z + cameraMargin);
            
            transform.position = fixedPosition;
        }

        #endregion

        protected override void OnCameraComponentConfig(CameraConfig config)
        {
            SetCameraPlatformImplementation(CameraMovementImpl.Create(config._platform));
            var terrainRenderer = config._terrain.GetComponent<Renderer>();
            var terrainRendererBounds = terrainRenderer.bounds;
            maxTerrainPosition = terrainRendererBounds.max;
            minTerrainPosition = terrainRendererBounds.min;
        }
    }
}