﻿using UnityEngine;

namespace Technical.Scripts.CameraConfig.Movement
{
    public class PcCameraMovementImpl : CameraMovementImpl
    {
        public override Vector3 GetMovementDirection(Transform cameraTransform)
        {
            float hSpeed =  Input.GetAxis("Horizontal");
            float vSpeed = Input.GetAxis("Vertical");

            Vector3 lateralMovement = hSpeed * cameraTransform.right;
            Vector3 forwardMovement = cameraTransform.forward;
            forwardMovement.y = 0f; //reset vertical component
            forwardMovement *= vSpeed;

            return lateralMovement + forwardMovement;
        }
    }
}