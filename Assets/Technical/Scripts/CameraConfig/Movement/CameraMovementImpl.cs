﻿using UnityEngine;

namespace Technical.Scripts.CameraConfig.Movement
{
    public abstract class CameraMovementImpl
    {
        public abstract Vector3 GetMovementDirection(Transform cameraTransform);
        
        public static CameraMovementImpl Create(CameraPlatform platform)
        {
            switch (platform)
            {
                case CameraPlatform.Mobile: return new MobileCameraMovementImpl();
                default: return new PcCameraMovementImpl();
            }
        }
    }
}