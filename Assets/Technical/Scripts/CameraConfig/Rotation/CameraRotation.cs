﻿using Technical.Scripts.Managers;
using UnityEngine;

namespace Technical.Scripts.CameraConfig.Rotation
{
    public class CameraRotation : CameraComponent<CameraRotationImpl>
    {
        [SerializeField] private Transform target;
        [SerializeField] private float angularSpeed = 2f;
        [SerializeField] private float minTouchMovementForRotation = 1f;
        
        private float angularDisplacement = 0f;

        #region Lifecycle
        private void Start()
        {
            if (!target) return;
            TouchUtil.minMovementDistance = minTouchMovementForRotation;
            LookAtTarget();
        }
        protected override void OnFixedUpdate(float elapsed)
        {
            if (!target) return;
            base.OnFixedUpdate(elapsed);
            angularDisplacement += platformImpl.GetRotation()  * elapsed * angularSpeed;
            if (angularDisplacement == 0f) return;
            LookAtTarget();
            transform.RotateAround(target.position, Vector3.up, angularDisplacement);
            angularDisplacement = 0f;
        }
        
        #endregion

        private void LookAtTarget()
        {
            Vector3 targetNormalized = target.position;
            targetNormalized.y = transform.position.y; //make sure position is at the same height of camera
            transform.LookAt(targetNormalized);
        }
        
        protected override void OnCameraComponentConfig(CameraConfig config)
        {
            SetCameraPlatformImplementation(CameraRotationImpl.Create(config._platform));
            var cameraSettings = DataManager.Instance.GetData().settings.cameraSettings;
            angularSpeed = cameraSettings.cameraRotationSpeed;
        }
    }
}