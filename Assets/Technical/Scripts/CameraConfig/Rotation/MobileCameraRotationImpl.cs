﻿using UnityEngine;

namespace Technical.Scripts.CameraConfig.Rotation
{
    public class MobileCameraRotationImpl : CameraRotationImpl
    {
        public override float GetRotation()
        {
            if (Input.touchCount != 1) return 0f;

            var touch = Input.GetTouch(0);
            
            if (TouchUtil.IsStandingStill(touch) || Mathf.Abs(touch.deltaPosition.x) < TouchUtil.minMovementDistance) return 0f;
            return touch.deltaPosition.x > 0f ? 1f : -1f;
        }
    }
}