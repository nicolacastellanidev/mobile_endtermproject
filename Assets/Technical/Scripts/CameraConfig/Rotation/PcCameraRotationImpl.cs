﻿using UnityEngine;

namespace Technical.Scripts.CameraConfig.Rotation
{
    public class PcCameraRotationImpl : CameraRotationImpl
    {
        public override float GetRotation()
        {
            return !Input.GetKey("mouse 1") ? 0f : Input.GetAxis("Mouse X");
        }
    }
}