﻿namespace Technical.Scripts.CameraConfig.Rotation
{
    public abstract class CameraRotationImpl
    {
        public abstract float GetRotation();
        
        public static CameraRotationImpl Create(CameraPlatform platform)
        {
            switch (platform)
            {
                case CameraPlatform.Mobile: return new MobileCameraRotationImpl();
                default: return new PcCameraRotationImpl();
            }
        }
    }
}