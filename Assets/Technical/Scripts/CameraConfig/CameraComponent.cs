﻿using System;
using UnityEngine;

namespace Technical.Scripts.CameraConfig
{
    [RequireComponent(typeof(RtsCameraController))]
    public abstract class CameraComponent<T> : MonoBehaviour
    {
        private T _impl;
        protected T platformImpl => _impl;
        protected bool IsEnabled => _impl != null;
        
        #region LifeCycle

        private void Update()
        {
            if (!IsEnabled) return;
            OnUpdate(Time.deltaTime);
        }

        private void FixedUpdate()
        {
            if (!IsEnabled) return;
            OnFixedUpdate(Time.fixedDeltaTime);
        }

        private void LateUpdate()
        {
            if (!IsEnabled) return;
            OnLateUpdate();
        }
        #endregion
        
        protected void SetCameraPlatformImplementation<W>(W impl) where W : T
        {
            _impl = impl;
        }

        public void CameraComponentConfig(CameraConfig config)
        {
            if (config._terrain == null)
            {
                Debug.LogWarning($"{gameObject.name} needs a Terrain in order to properly execute its logic");
                Destroy(this);
                return;
            }
            OnCameraComponentConfig(config);
        }
        
        protected abstract void OnCameraComponentConfig(CameraConfig config);

        protected virtual void OnUpdate(float elapsed) { }
        protected virtual void OnFixedUpdate(float elapsed) { }
        protected virtual void OnLateUpdate() { }
    }
}