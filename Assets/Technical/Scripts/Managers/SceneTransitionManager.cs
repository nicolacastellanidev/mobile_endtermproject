﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Technical.Scripts.Managers
{
    public class SceneTransitionManager : MonoBehaviour
    {
        public static SceneTransitionManager Instance = null;
        [SerializeField] private string mainMenuSceneName = "MainMenu";
        private string currentScene;
        
        public Action<string, bool> SceneTransitionStart;
        public Action<string, bool> SceneTransitionEnd;

        private AsyncOperation async;
        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
                SceneManager.sceneLoaded += OnSceneLoaded;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        public void LoadScene(string sceneName, bool showLoader = false)
        {
            StartCoroutine(LoadSceneAsync(sceneName, showLoader));
        }

        public void BackToMenu()
        {
            LoadScene(mainMenuSceneName);
        }
        
        public void ReloadCurrentScene()
        {
            LoadScene(SceneManager.GetActiveScene().name);
        }

        private IEnumerator LoadSceneAsync(string sceneName, bool showLoader = false)
        {
            SceneTransitionStart?.Invoke(sceneName, mainMenuSceneName == sceneName);
            if (showLoader) yield return new WaitForSeconds(1.5f); //cap loader waiting time
            async = SceneManager.LoadSceneAsync(sceneName);
            yield return async;
        }
        
        private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
            currentScene = scene.name;
            SceneTransitionEnd?.Invoke(currentScene, mainMenuSceneName == currentScene);
        }
    }
}