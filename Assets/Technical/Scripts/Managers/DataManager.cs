﻿using System;
using System.IO;
using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Managers
{
    public class DataManager
    {
        public static DataManager Instance = new DataManager();
        public UnityAction<PersistentData> DataUpdated;
        private PersistentData data;

        private DataManager()
        {
            data = LoadData();
        }
        
        public PersistentData GetData()
        {
            return data;
        }
        
        public void SetDataAndSave(PersistentData next)
        {
            data = next;
            DataUpdated?.Invoke(data);
            SaveData(data);
        }
        
        public void SetData(PersistentData next)
        {
            data = next;
            DataUpdated?.Invoke(data);
        }
        
        public GameData GetGameData()
        {
            return Instance.data.gameData;
        }
        /// <summary>
        /// Load data from persistent data path
        /// </summary>
        /// <param name="testMode">bool</param>
        /// <returns>PlayerData</returns>
        public PersistentData LoadData()
        {
            var filepath = Application.persistentDataPath + "/game_data.json";
            Debug.Log($"Getting data from path: {filepath}");
            var readJson = "";
            try
            {
                using (var sr = new StreamReader(filepath))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        readJson += line;
                    }
                }
            }
            catch (FileNotFoundException){
                var data = new PersistentData();
                SaveData(data);
                return data;
            }
            return readJson != "" ? JsonUtility.FromJson<PersistentData>(readJson) : new PersistentData();
        }

        public void SaveData()
        {
            SaveData(data);
        }

        private bool SaveData(PersistentData data)
        {
            var filepath = Application.persistentDataPath + "/game_data.json";
            Debug.Log($"Saving data to path: {filepath}");
            try
            {
                var saveJson = JsonUtility.ToJson(data);
                using (var sw = new StreamWriter(filepath))
                {
                    sw.WriteLine(saveJson);
                }
            }
            catch (Exception e)
            {
                Debug.LogError($"An error has occurred while saving data: {e}");
                return false;
            }
            return true;
        }
    }
}