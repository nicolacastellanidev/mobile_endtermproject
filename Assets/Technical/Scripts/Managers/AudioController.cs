﻿using System.Collections;
using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.Audio;

namespace Technical.Scripts.Managers
{
    public class AudioController : MonoBehaviour
    {
        [SerializeField] private AudioMixer mixer;
        
        private AudioSource[] _audioSources;

        private void Awake()
        {
            _audioSources = GetComponents<AudioSource>();
        }

        private void OnSceneTransitionEnd(string sceneName, bool backToMain)
        {
            if (backToMain)
            {
                PlayInMenuMusic();
            }
            else
            {
                PlayInGameMusic();
            }
        }

        private void Start()
        {
            OnAudioDataUpdate(DataManager.Instance.GetData());
            DataManager.Instance.DataUpdated += OnAudioDataUpdate;
            OnAudioDataUpdate(DataManager.Instance.GetData());
            SceneTransitionManager.Instance.SceneTransitionEnd += OnSceneTransitionEnd;
            
            _audioSources[0].Play();
        }

        private void OnAudioDataUpdate(PersistentData data)
        {
            mixer.SetFloat("MasterVolume", data.settings.volumeSettings.masterVolume);
            mixer.SetFloat("MusicVolume", data.settings.volumeSettings.musicVolume);
            mixer.SetFloat("SfxVolume", data.settings.volumeSettings.sfxVolume);
            mixer.SetFloat("UiVolume", data.settings.volumeSettings.uiVolume);
        }
        
        private void PlayInGameMusic()
        {
            StartCoroutine(FadeOutVolume(_audioSources[0]));
            StartCoroutine(FadeInVolume(_audioSources[1]));
        }

        private void PlayInMenuMusic()
        {
            StartCoroutine(FadeOutVolume(_audioSources[1]));
            StartCoroutine(FadeInVolume(_audioSources[0]));
        }
        
        private IEnumerator FadeOutVolume(AudioSource audioSource)
        {
            while (audioSource.volume > 0f)
            {
                audioSource.volume -= 0.1f;
                yield return new WaitForSeconds(0.05f);
            }
            audioSource.Stop();
        }
        private IEnumerator FadeInVolume(AudioSource audioSource)
        {
            audioSource.Play();
            while (audioSource.volume < 1f)
            {
                audioSource.volume += 0.1f;
                yield return new WaitForSeconds(0.05f);
            }
        }
    }
}
