﻿using System;
using System.Collections;
using System.Linq;
using Technical.Scripts.GameMode;
using Technical.Scripts.Models;
using UnityEngine;

namespace Technical.Scripts.Managers
{
    public class ScoreManager : MonoBehaviour
    {
        [Header("Score parameters"), Space(10)]
        [SerializeField] private float minValuableTimePercent = 0.8f;
        [SerializeField] private int maxSpawnableUnitsForStar = 10;

        public Action<int> onScoreUpdate;
        
        private float timeForScore = 0f;
        private Coroutine updateUnitOccupationScoreRoutine = null;
        
        public static ScoreManager Instance;
        
        // 0 == unit_occupation, 1 == time, 2 == victory
        private readonly int[] mScore = {1, 1, 1};

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            } else if (Instance != this)
            {
                Destroy(this);
            }
        }

        private void Start()
        {
            float gameTime = GameModeController.Instance.gameDurationInSeconds;
            timeForScore = gameTime - (gameTime * minValuableTimePercent);

            GameModeController.Instance.onPlayerUnitUpdate += OnPlayerUnitUpdate;
            GameModeController.Instance.onTimeUpdate += UpdateTimeScore;
        }

        private void OnPlayerUnitUpdate(Player player)
        {
            if (player.Team == Team.AI) return;
            if(updateUnitOccupationScoreRoutine != null)
                StopCoroutine(updateUnitOccupationScoreRoutine);
            updateUnitOccupationScoreRoutine = StartCoroutine(UpdateOccupiedUnitsScore(player.unitOccupation));
        }

        public int GetScore()
        {
            return mScore.Sum();
        }
        
        private IEnumerator UpdateOccupiedUnitsScore(int occupiedUnits)
        {
            //delay to intercept create and destruct unit for replacing the content of a cell
            yield return new WaitForSeconds(0.15f); 
            if (occupiedUnits <= maxSpawnableUnitsForStar)
            {
                mScore[0] = 1;
            }
            else
            {
                mScore[0] = 0;
            }
            onScoreUpdate?.Invoke(GetScore());
        }
        
        private void UpdateTimeScore(float remainingGameTime)
        {
            if (remainingGameTime > timeForScore)
            {
                mScore[1] = 1;
            }
            else
            {
                mScore[1] = 0;
            }
            onScoreUpdate?.Invoke(GetScore());
        }
    }
}
