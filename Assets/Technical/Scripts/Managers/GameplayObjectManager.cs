﻿using System.Collections.Generic;
using System.Linq;
using Technical.Scripts.Controllers;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.GameMode;
using Technical.Scripts.Models;
using UnityEngine;

namespace Technical.Scripts.Managers
{
    public class GameplayObjectManager : MonoBehaviour
    {
        public static GameplayObjectManager Instance;

        private void Awake()
        {
            if (Instance && Instance != this)
            {
                Destroy(this);
                return;
            }

            Instance = this;
        }

        public void RegisterGPO(GameplayObjectController gpo, Team team)
        {
            var player = GetPlayerByTeam(team);
            player.RegisterGPO(gpo);
        }

        public void UnregisterGPO(GameplayObjectController gpo, Team team)
        {
            var player = GetPlayerByTeam(team);
            player.UnregisterGPO(gpo);
        }

        public GameObject GetDefaultTargetFor(GameplayObjectController gpo)
        {
            if (!GameModeController.Instance) return null;
            
            switch (GameModeController.Instance.GetCurrentMode())
            {
                case GameModeType.SIMULATION:
                    return gpo.GetModel() == GameplayObjectModel.Priest 
                        ? GetNextTargetForPriest(gpo) // find the nearest ally to your nearest enemy
                        : GetNearestEnemy(gpo);
                default: return null;
            }
        }

        private GameObject GetNextTargetForPriest(GameplayObjectController gpo)
        {
            var nearestEnemy = GetNearestEnemy(gpo);
            var nearestAllies = GetNearestEnemies(nearestEnemy.GetComponent<GameplayObjectController>());
            if (nearestAllies.Length == 0) return gpo.gameObject;
            var firstTargets = nearestAllies.OrderByDescending(ally =>
            {
                var model = ally.GetComponent<GameplayObjectController>().GetModel();
                return model != GameplayObjectModel.Ballista && model != GameplayObjectModel.Priest &&
                       model != GameplayObjectModel.Turret;
            });

            return firstTargets.FirstOrDefault();
        }

        private GameObject[] GetNearestEnemies(GameplayObjectController gpo)
        {
            return GetNearestGPOsByPlayer( GetPlayerByTeam( gpo.GetTeam() == Team.PLAYER ? Team.AI : Team.PLAYER ), gpo );
        }
        
        private GameObject[] GetFarestEnemies(GameplayObjectController gpo)
        {
            return GetFarestGPOsByPlayer( GetPlayerByTeam( gpo.GetTeam() == Team.PLAYER ? Team.AI : Team.PLAYER ), gpo );
        }

        public GameObject GetNearestEnemy(GameplayObjectController gpo)
        {
            return GetNearestGPOByPlayer( GetPlayerByTeam( gpo.GetTeam() == Team.PLAYER ? Team.AI : Team.PLAYER ), gpo );
        }
        
        public GameObject GetNearestAlly(GameplayObjectController gpo)
        {
            return GetNearestGPOByPlayer( GetPlayerByTeam( gpo.GetTeam() ), gpo );
        }
        
        public GameObject GetFarestAlly(GameplayObjectController gpo)
        {
            return GetFarestGpoByPlayer( GetPlayerByTeam( gpo.GetTeam() ), gpo );
        }

        public void ActivateAll()
        {
            var gos = new List<GameplayObjectController>();
            
            // get all gpos
            gos.AddRange(GameModeController.Instance.GetPlayerByTeam(Team.PLAYER).GetAllGPO());
            gos.AddRange(GameModeController.Instance.GetPlayerByTeam(Team.AI).GetAllGPO());
            
            // activate them all
            
            foreach (var gameplayObjectController in gos)
            {
                gameplayObjectController.Activate();
            }
        }
        
        public void SleepAll()
        {
            var gos = new List<GameplayObjectController>();
            
            // get all gpos
            gos.AddRange(GameModeController.Instance.GetPlayerByTeam(Team.PLAYER).GetAllGPO());
            gos.AddRange(GameModeController.Instance.GetPlayerByTeam(Team.AI).GetAllGPO());
            
            // activate them all
            
            foreach (var gameplayObjectController in gos)
            {
                gameplayObjectController.Sleep();
            }
        }

        private GameObject GetNearestGPOByPlayer(Player player, GameplayObjectController gpo)
        {
            var target = player.GetAllGPO()
                .Where(nextGpo => nextGpo != gpo)
                .OrderBy(c => (c.transform.position - gpo.transform.position).sqrMagnitude)
                .FirstOrDefault();
            
            return target ? target.gameObject : null;
        }
        
        private GameObject[] GetNearestGPOsByPlayer(Player player, GameplayObjectController gpo)
        {
            var targets = player.GetAllGPOGameObject()
                .Where(nextGpo => nextGpo != gpo)
                .OrderBy(c => (c.transform.position - gpo.transform.position).sqrMagnitude);
            
            return targets.ToArray();
        }
        
        private GameObject[] GetFarestGPOsByPlayer(Player player, GameplayObjectController gpo)
        {
            var targets = player.GetAllGPOGameObject()
                .Where(nextGpo => nextGpo != gpo)
                .OrderByDescending(c => (c.transform.position - gpo.transform.position).sqrMagnitude);
            
            return targets.ToArray();
        }
        
        private GameObject GetFarestGpoByPlayer(Player player, GameplayObjectController gpo)
        {
            var target = player.GetAllGPO()
                .Where(nextGpo => nextGpo != gpo)
                .OrderBy(c => (c.transform.position - gpo.transform.position).sqrMagnitude)
                .Reverse()
                .FirstOrDefault();
            
            return target ? target.gameObject : null;
        }

        private Player GetPlayerByTeam(Team search)
        {
            return GameModeController.Instance.GetPlayerByTeam(search);
        }
    }
}
