﻿using Technical.Scripts.Controllers;
using Technical.Scripts.Controllers.Base;
using UnityEngine;

namespace Technical.Scripts.Spells
{
    public class BallistaProjectileDamageAoeSpell : DamageAoeSpell
    {
        [SerializeField, Range(1f, 100f)] private float structuresDamageMultiplier = 1f;

        public override void Execute(GameObject[] targets, float amount)
        {
            if (targets == null) return;
            foreach (var target in targets)
            {
                float hitAmount = target.GetComponent<GameplayObjectController>()?.IsStructure() == true
                    ? amount * structuresDamageMultiplier
                    : amount;
                target.GetComponent<BaseHealthController>()?.Hit(hitAmount);
            }
        }
    }
}