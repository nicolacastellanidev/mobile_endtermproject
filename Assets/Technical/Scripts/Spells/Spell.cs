﻿using UnityEngine;

namespace Technical.Scripts.Spells
{
    public class Spell: MonoBehaviour
    {
        public virtual void Execute(GameObject[] targets, float amount) {}
    }
}