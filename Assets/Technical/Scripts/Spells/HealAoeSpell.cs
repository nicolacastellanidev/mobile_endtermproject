﻿using System.Collections;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Spells;
using UnityEngine;

namespace Technical.Scripts.Commands.Spells
{
    /// <summary>
    /// Issues about heal spell:
    /// 1. If currents targets aren't in range, the spell still heal them
    /// 2. If caster dies, heals continues
    /// 3. Heal spell doesn't move with caster
    /// </summary>
    public class HealAoeSpell : Spell
    {
        [SerializeField] private float amountStep = 10f;
        [SerializeField] private float executeEachSeconds = 0.25f;
        private float currentAmount = 0;
        
        public override void Execute(GameObject[] targets, float amount)
        {
            if (targets == null) return;
            StartCoroutine(ExecuteSpellTimed(targets, amount));
        }

        private IEnumerator ExecuteSpellTimed(GameObject[] targets, float amount)
        {
            // if (targets == null) yield break;

            while (currentAmount < amount)
            {
                foreach (var target in targets)
                {
                    if (target && target.activeInHierarchy) target.GetComponent<BaseHealthController>()?.Heal(amountStep);
                }

                currentAmount += amountStep;
                yield return new WaitForSeconds(executeEachSeconds);
            }

            FadeOut();
        }

        private void FadeOut()
        {
            StartCoroutine(FadeOutCoroutine());
        }

        private IEnumerator FadeOutCoroutine()
        {
            while (transform.localScale != Vector3.zero)
            {
                transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, 0.1f);
                yield return new WaitForEndOfFrame();
            }
            
            Destroy(gameObject);
        }
    }
}
