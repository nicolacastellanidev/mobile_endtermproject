﻿using Technical.Scripts.Controllers.Base;
using UnityEngine;

namespace Technical.Scripts.Spells
{
    public class DamageAoeSpell : Spell
    {
        public override void Execute(GameObject[] targets, float amount)
        {
            if (targets == null) return;
            foreach (var target in targets)
            {
                target.GetComponent<BaseHealthController>()?.Hit(amount);
            }
        }
    }
}
