﻿using System;
using Technical.Scripts.Models;
using Technical.Scripts.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.GameMode.SimulationMode
{
    public class SimulationModeBoardController: MonoBehaviour
    {
        [SerializeField] private int rows = 10;
        [SerializeField] private int columns = 10;
        [SerializeField] private float gridGap = 2f;
        [SerializeField] private GameObject team1CellPrefab;
        [SerializeField] private GameObject team2CellPrefab;
        
        [Serializable] public class CellSelectedEvent: UnityEvent<GridCellController> {}
        [SerializeField] private CellSelectedEvent cellSelected;

        private GameObject[,] team1Grid;
        private GameObject[,] team2Grid;

        private GridCellController _selectedCell;

        private void Awake()
        {
            team1Grid = GridGenerator.GenerateGrid(rows, columns, team1CellPrefab, -1, gridGap);
            team2Grid = GridGenerator.GenerateGrid(rows, columns, team2CellPrefab, 1, gridGap);

            SubscribeToEvents(team1Grid);
        }

        private void SubscribeToEvents(GameObject[,] grid)
        {
            for (var row = 0; row < grid.GetLength(0); row++)
            {
                for (var col = 0; col < grid.GetLength(1); col++)
                {
                    var controller = grid[row, col].GetComponent<GridCellController>();
                    controller.onSelectionChange += OnCellSelectionChange;
                    controller.onGameplayObjectReplace += OnCellGameplayObjectChange;
                }
            }
        }

        private void OnCellGameplayObjectChange(GridCellController cell)
        {
            GameModeController.Instance.DestroyGameplayObject(cell.gameplayObject, Team.PLAYER);
        }

        private void OnCellSelectionChange(GridCellController cell)
        {
            if (cell == null)
            {
                _selectedCell = cell;
                return;
            }
            
            if (_selectedCell)
            {
                if (_selectedCell == cell) return;
                _selectedCell.Deselect();
            }

            _selectedCell = cell;
            cellSelected?.Invoke(cell);
        }

        public void DeselectCell()
        {
            if (_selectedCell)
            {
                _selectedCell.Deselect();
            }
            _selectedCell = null;
            cellSelected?.Invoke(_selectedCell);
        }

        public GameObject[,] GetGridForTeam(Team team)
        {
            return team == Team.PLAYER ? team1Grid : team2Grid;
        }

        public bool AnyFreeCellForTeam(Team team)
        {
            var grid = GetGridForTeam(team);
            bool anyFreeCell = false;
            for (var row = 0; row < grid.GetLength(0) && !anyFreeCell; row++)
            {
                for (var col = 0; col < grid.GetLength(1) && !anyFreeCell; col++)
                {
                    var controller = grid[row, col].GetComponent<GridCellController>();
                    anyFreeCell = controller != null && controller.Free();
                }
            }
            return anyFreeCell;
        }
        
        public void DisableAllSelectableCells()
        {
            DisableAllSelectableCellsForTeam(Team.PLAYER);
            DisableAllSelectableCellsForTeam(Team.AI);
        }

        public void DisableAllSelectableCellsForTeam(Team team)
        {
            var grid = GetGridForTeam(team);
            for (var row = 0; row < grid.GetLength(0); row++)
            {
                for (var col = 0; col < grid.GetLength(1); col++)
                {
                    var controller = grid[row, col].GetComponent<GridCellController>();
                    if (controller && controller.selectable)
                    {
                        controller.selectable = false;
                    }
                }
            }
        }
    }
}