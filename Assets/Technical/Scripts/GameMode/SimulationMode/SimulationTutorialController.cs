﻿using System;
using Technical.Scripts.Models;
using Technical.Scripts.Models.SimulationMode;
using Technical.Scripts.UI;
using UnityEditor;
using UnityEngine;

namespace Technical.Scripts.GameMode.SimulationMode
{
    public class SimulationTutorialController : MonoBehaviour
    {
        [SerializeField] private SimulationTutorialStep[] steps;
        private int currentStepIndex = 0;

        public void ShowTutorial()
        {
            ShowStep(currentStepIndex);
        }

        private void ShowStep(int i)
        {
            currentStepIndex = i;
            if (currentStepIndex < 0 || currentStepIndex == steps.Length)
            {
                if (currentStepIndex == steps.Length)
                {
                    // tutorial done
                    PlayerPrefs.SetInt("skip_tutorial", 1);
                }

                currentStepIndex = 0;
                return;
            }
            Popup.Instance
                .builder()
                .WithTitle("Tutorial")
                .WithMessage(steps[i].message)
                .WithImage(steps[i].image)
                .WithAlternativeButton(() => { PlayerPrefs.SetInt("skip_tutorial", 1); }, "Skip")
                .WithPositiveButton(() => { ShowStep(currentStepIndex + 1); }, steps[i].nextButtonLabel)
                .WithNegativeButton(() => { ShowStep(currentStepIndex - 1); }, steps[i].previousButtonLabel)
                .Show();
        }
    }
}
