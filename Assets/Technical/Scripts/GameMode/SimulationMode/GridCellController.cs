﻿using System;
using System.Collections.Generic;
using Technical.Scripts.Controllers;
using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Technical.Scripts.GameMode.SimulationMode
{
    /// <summary>
    /// Still a prototype, handles click on cell
    /// </summary>
    public class GridCellController : MonoBehaviour
    {
        [SerializeField] private Team team;
        public bool selectable;
        
        public Action<GridCellController> onSelectionChange;
        public Action<GridCellController> onGameplayObjectReplace;
        
        private SpriteRenderer sr;
        private Color originalColor;

        [HideInInspector] public GameplayObjectController gameplayObject;
        
        private void Awake()
        {
            sr = GetComponentInChildren<SpriteRenderer>();
            originalColor = sr.color;
            SetAlpha(0.5f);
        }

        private void OnMouseDown()
        {
            if (!selectable || IsPointerOverUIObject()) return;
            Select();
        }
        
        private bool IsPointerOverUIObject()
        {
            var position = Application.isEditor || SystemInfo.deviceType == DeviceType.Desktop ? (Vector2)Input.mousePosition : Input.GetTouch(0).position;
            var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(position.x, position.y);
            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        public override bool Equals(object other)
        {
            return other != null && other.GetType() == GetType() && other.ToString() == ToString();
        }

        public override string ToString()
        {
            return gameObject.name;
        }

        public void Deselect()
        {
            SetAlpha(0.5f);
            onSelectionChange?.Invoke(null);
        }

        private void Select()
        {
            SetAlpha(1);
            onSelectionChange?.Invoke(this);
        }

        public bool Free()
        {
            return gameplayObject == null;
        }

        public void SetGameplayObjectController(GameplayObjectController controller)
        {
            if (controller == null)
            {
                if (!gameplayObject) return;
                onGameplayObjectReplace.Invoke(this);
                gameplayObject = null;
                return;
            }
            if (gameplayObject != null)
            {
                onGameplayObjectReplace?.Invoke(this);
            }
            gameplayObject = controller;
        }

        private void SetAlpha(float amount)
        {
            var color = originalColor;
            color.a = amount;
            sr.color = color;
        }
    }
}
