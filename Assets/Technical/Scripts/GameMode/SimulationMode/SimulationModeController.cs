using System;
using Technical.Scripts.GameMode.IA;
using Technical.Scripts.Managers;
using Technical.Scripts.Models;
using Technical.Scripts.UI;
using UnityEngine;

namespace Technical.Scripts.GameMode.SimulationMode
{
    [RequireComponent(typeof(SimulationModeBoardController))]
    [RequireComponent(typeof(IaSimulationController))]
    public class SimulationModeController : GameModeController
    {
        [SerializeField] private GameObject starsWrapperPrefab;
        
        [Header("Sound settings"), Space(10)]
        [SerializeField] private AudioClip WinSound;
        [SerializeField] private AudioClip LoseSound;

        private SimulationModeBoardController boardController;
        private IaSimulationController iaController;
        private AudioSource audioSource;
        private SimulationTutorialController tutorialController;

        #region Lifecycle

        protected override void Awake()
        {
            base.Awake();
            boardController = GetComponent<SimulationModeBoardController>();
            iaController = GetComponent<IaSimulationController>();
            audioSource = GetComponent<AudioSource>();
            tutorialController = GetComponent<SimulationTutorialController>();
            Time.timeScale = 1f;
        }

        private void Start()
        {
            iaController.SpawnBatch();
            _isGameRunning = false;
            
            Popup.Instance
                .builder()
                .WithTitle(Settings.startTitle)
                .WithMessage(Settings.startText)
                .WithPositiveButton(() =>
                {
                    if (PlayerPrefs.GetInt("skip_tutorial") != 1)
                    {
                        tutorialController.ShowTutorial();
                    }
                })
                .Show();

            remainingGameTime = gameDurationInSeconds;
            onTimeUpdate?.Invoke(remainingGameTime);
        }

        private void Update()
        {
            if (!_isGameRunning || _isGamePaused) return;
            UpdateGameTime(Time.deltaTime);
            if (players[0].GetAllGPO().Count == 0)
            {
                // player 1 lose
                EndGame();
                return;
            }

            if (players[1].GetAllGPO().Count != 0) return;
            // player 1 wins
            EndGame(true);
        }

        #endregion

        #region Public Interface
        public override GameModeType GetCurrentMode() => GameModeType.SIMULATION;

        public GameObject[,] GetGridForTeam(Team team)
        {
            return boardController.GetGridForTeam(team);
        }

        public bool AnyFreeCellInGridForTeam(Team team)
        {
            return boardController.AnyFreeCellForTeam(team);
        }
        
        public override void StartGame()
        {
            boardController.DisableAllSelectableCells();
            GameplayObjectManager.Instance.ActivateAll();
            _isGameRunning = true;
            boardController.DeselectCell();
            audioSource.Play();
            onGameStart?.Invoke();
        }

        public override void EndGame(bool win = false)
        {
            _isGameRunning = false;
            onGameEnd?.Invoke();
            GameplayObjectManager.Instance.SleepAll();
            audioSource.Stop();
            if (win)
            {
                if (!skipLevelSave && ScoreManager.Instance != null)
                {
                    SaveLevelScore();
                }

                ShowWinPopup();
            }
            else
            {
                ShowLosePopup();
            }
        }
        
        public override void Pause()
        {
            Action onButtonPressed = () => { 
                _isGamePaused = false;
                Time.timeScale = 1f;
                if(_isGameRunning) audioSource.Play();
            };
            
            _isGamePaused = true;
            
            if(_isGameRunning) audioSource.Pause();
            
            Popup.Instance
                .builder()
                .WithTitle("Game Paused")
                .WithMessage("Also great warriors take a pause sometimes...")
                .WithNegativeButton(() =>
                {
                    onButtonPressed.Invoke();
                    SceneTransitionManager.Instance?.ReloadCurrentScene();
                }, "Retry")
                .WithAlternativeButton(() =>
                {
                    onButtonPressed.Invoke();   
                    SceneTransitionManager.Instance?.BackToMenu();
                }, "Menu")
                .WithPositiveButton(() =>
                {
                    onButtonPressed.Invoke();
                }, "Resume")
                .AddOnShowAction(() => { Time.timeScale = 0f;})
                .Show();
        }

        #endregion
        
        #region Private 
        private void UpdateGameTime(float elapsed)
        {
            remainingGameTime -= elapsed;
            onTimeUpdate?.Invoke(remainingGameTime);
            if (!HasTimeRunOut) return;

            var playerGosCount = players[0].GetAllGPO().Count;
            var aiGosCount = players[1].GetAllGPO().Count;
            // end game based on unit count
            EndGame(playerGosCount <= aiGosCount);
        }
        
        private void SaveLevelScore()
        {
            if (skipLevelSave) return;
            DataManager.Instance.GetGameData().LevelComplete(Settings.levelId, ScoreManager.Instance.GetScore());
            DataManager.Instance.SaveData();
        }

        private void ShowWinPopup()
        {
            var popupBuilder = Popup.Instance
                .builder()
                .WithTitle("Game Ended!")
                .WithMessage("Congratulations! You won the battle!")
                .WithAudioClip(WinSound)
                .WithNegativeButton(() => { SceneTransitionManager.Instance?.ReloadCurrentScene(); }, "Retry")
                .WithAlternativeButton(() => SceneTransitionManager.Instance?.BackToMenu(), "Menu");

            var nextLevelScene = DataManager.Instance.GetGameData().GetLevel(Settings.levelId)?.nextLevelId;

            if (!string.IsNullOrEmpty(nextLevelScene))
            {
                popupBuilder.WithPositiveButton(() => { SceneTransitionManager.Instance?.LoadScene(nextLevelScene); },"Next level");
            }
            
            var popupContent = skipLevelSave ? null : GenerateStars();
            if (popupContent != null)
            {
                Action onShowPopupAction = () => popupContent.GetComponent<UIAnimatedStarsWrapperController>()
                    .UpdateStarsAnimTimed(ScoreManager.Instance.GetScore());

                popupBuilder
                    .WithContent(popupContent)
                    .AddOnShowAction(onShowPopupAction);
            }
            
            popupBuilder.Show();
        }
        
        private void ShowLosePopup()
        {
            Popup.Instance
                .builder()
                .WithTitle("Game Ended!")
                .WithMessage("Oh no, you lose!")
                .WithNegativeButton(() => { SceneTransitionManager.Instance?.ReloadCurrentScene(); }, "Retry")
                .WithAlternativeButton(() => { SceneTransitionManager.Instance?.BackToMenu(); }, "Menu")
                .WithAudioClip(LoseSound)
                .Show();
        }

        private GameObject GenerateStars()
        {
            var starsWrapper = Instantiate(starsWrapperPrefab, Vector3.zero, Quaternion.identity);
            return starsWrapper;
        }

        #endregion
    }
}