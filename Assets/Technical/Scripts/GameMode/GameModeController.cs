﻿using System;
using Technical.Scripts.Controllers;
using Technical.Scripts.Managers;
using Technical.Scripts.Models;
using UnityEngine;

namespace Technical.Scripts.GameMode
{
    [Serializable]
    public enum GameModeType
    {
        SIMULATION,
        DEFENCE,
        KOH,
        NONE
    }
    
    public abstract class GameModeController : MonoBehaviour
    {
        public static GameModeController Instance;
        
        // LIFE MANAGEMENT
        public Action onGameStart;
        public Action onGameEnd;

        // TIME MANAGEMENT
        public Action<Player> onPlayerUnitUpdate;
        public Action<float> onTimeUpdate;
        public float gameDurationInSeconds = 120f;
        protected float remainingGameTime;
        protected bool HasTimeRunOut => remainingGameTime <= 0;
        
        [Header("Level settings"), Space(10)]
        [SerializeField] private GameModeSettings settings;
        public GameModeSettings Settings => settings;
        [SerializeField] protected bool skipLevelSave = false;
        
        protected readonly Player[] players = { new Player(Team.PLAYER), new Player(Team.AI) };

        protected bool _isGameRunning = false;
        protected bool _isGamePaused = false;
        
        protected virtual void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(this);
            }
        }

        public virtual GameModeType GetCurrentMode()
        {
            return GameModeType.NONE;
        }
        
        public Player GetPlayerByTeam(Team search)
        {
            return search == Team.PLAYER ? players[0] : players[1];
        }
        
        public GameplayObjectController SpawnUnit(GameplayObjectModel model, Team team, Vector3 position, Quaternion rotation)
        {
            var result = settings.playerUnitSettings.GetPropertiesByModel(model);
            if (result is null) return null;
            // cast to non-nullable value
            var props = (PlayerUnitSettingsProperties) result;
            var player = GetPlayerByTeam(team);
            
            if (player.unitOccupation + props.cost > settings.maxSpawnableUnits) return null;
            player.unitOccupation += props.cost;

            var go = Instantiate(props.GetPrefabForTeam(team), position, rotation);
            
            onPlayerUnitUpdate?.Invoke(player);
            return go.GetComponent<GameplayObjectController>();
        }
        
        public void DestroyGameplayObject(GameplayObjectController gameplayObject, Team team)
        {
            var model = gameplayObject.GetModel();
            var result = settings.playerUnitSettings.GetPropertiesByModel(model);
            if (result is null) return;
            // cast to non-nullable value
            var props = (PlayerUnitSettingsProperties) result;
            var player = GetPlayerByTeam(team);
            player.unitOccupation -= props.cost;
            onPlayerUnitUpdate?.Invoke(player);
            Destroy(gameplayObject.gameObject);
        }

        public virtual void StartGame()
        {
            Debug.Log("[START GAME] - Not implemented.");
        }
        
        public virtual void EndGame(bool win = false)
        {
            Debug.Log("[END GAME] - Not implemented.");
        }

        public virtual void Pause()
        {
            Debug.Log("[PAUSE GAME] - Not implemented.");

        }

    }
}