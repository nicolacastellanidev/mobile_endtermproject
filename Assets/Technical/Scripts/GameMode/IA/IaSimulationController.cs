﻿using System;
using System.Collections.Generic;
using System.Linq;
using Technical.Scripts.GameMode.SimulationMode;
using Technical.Scripts.Models;
using Technical.Scripts.Models.SimulationMode;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Technical.Scripts.GameMode.IA
{
    [RequireComponent(typeof(SimulationModeController))]
    public class IaSimulationController : MonoBehaviour
    {
        [SerializeField] private bool godMode = false;
        
        [Header("Spawn Configuration")]
        [SerializeField] private List<IaSimulationSpawnable> availableUnits;

        private SimulationModeController gameMode;

        private Vector2 lastFreeCellIndex = Vector2.zero;

        #region Lifecycle
        private void Awake()
        {
            gameMode = GetComponent<SimulationModeController>();
        }
        #endregion

        public void SpawnBatch()
        {
            //always reset every generation
            var board = gameMode.GetGridForTeam(Team.AI);
            var aiPlayer = gameMode.GetPlayerByTeam(Team.AI);
            
            lastFreeCellIndex.y = Random.Range(0, board.GetLength(0));
            lastFreeCellIndex.x = Random.Range(0, board.GetLength(1));

            var forcedUnit = availableUnits.Where(aU => aU.forceSpawn).ToList();

            if (forcedUnit.Any())
            {
                // evade forced units first
                foreach (var spawnable in forcedUnit)
                {
                    var cell = GetCellAt(spawnable.spawnPosition, board);
                    SpawnUnit(cell, spawnable);
                }
            }

            var unitLimit = (godMode ? 1 : gameMode.Settings.maxSpawnableUnits);
            // get random free cell
            while (gameMode.AnyFreeCellInGridForTeam(Team.AI) && aiPlayer.unitOccupation < unitLimit)
            {
                var cell = GetFreeCell(board);
                if (!cell && aiPlayer.unitOccupation < unitLimit) continue;
                if (!SpawnUnit(cell))
                {
                    // player can't spawn more unit
                    return;
                }
            }
        }
        
        public GridCellController GetFreeCell(GameObject[,] board)
        {
            GridCellController controller = null;
            for (var row = lastFreeCellIndex.y; row < board.GetLength(0) && !controller; row++)
            {
                for (var col = lastFreeCellIndex.x; col < board.GetLength(1) && !controller; col++)
                {
                    controller = board[(int)row, (int)col].GetComponent<GridCellController>();
                    if (!controller.Free()) controller = null;
                }
            }
            lastFreeCellIndex.y = Random.Range(0, board.GetLength(0));
            lastFreeCellIndex.x = Random.Range(0, board.GetLength(1));
            return controller;
        }
        
        public GridCellController GetCellAt(Vector2 position, GameObject[,] board)
        {
            var controller = board[(int)position.x, (int)position.y].GetComponent<GridCellController>();
            if (!controller.Free()) controller.SetGameplayObjectController(null);
            lastFreeCellIndex.y = Random.Range(0, board.GetLength(0));
            lastFreeCellIndex.x = Random.Range(0, board.GetLength(1));
            return controller;
        }

        private bool SpawnUnit(GridCellController cell, IaSimulationSpawnable forcedUnit = null)
        {
            var maxWeight = availableUnits.Sum(config => config.spawnProbability );
            var spawnProbability = Random.Range(0, maxWeight);

            var next = forcedUnit;
            
            for (var i = 0; i < availableUnits.Count && next == null; ++i)
            {
                if (spawnProbability < availableUnits[i].spawnProbability)
                    next = availableUnits[i];
                spawnProbability -= availableUnits[i].spawnProbability;
            }
            
            if (next is null) return true;
            try
            {
                var goc = GameModeController.Instance.SpawnUnit(next.model, Team.AI, cell.transform.position,
                    cell.transform.rotation);
                if (goc)
                {
                    cell.SetGameplayObjectController(goc);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException exception)
            {
                
                Debug.LogWarning(exception.ToString());
                return false;
            }
        }
    }
}