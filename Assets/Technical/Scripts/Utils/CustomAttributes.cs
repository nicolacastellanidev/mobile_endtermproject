﻿namespace Technical.Scripts.Utils
{
    /// <summary>
    /// This function is called by an animation event
    /// </summary>
    public class CalledByAnimationEvent : System.Attribute {}
}