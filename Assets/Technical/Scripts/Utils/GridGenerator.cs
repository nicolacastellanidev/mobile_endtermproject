﻿using UnityEngine;

namespace Technical.Scripts.Utils
{
    public static class GridGenerator
    {
        public static GameObject[,] GenerateGrid(int rows, int columns, GameObject prefab, float direction = 1,
            float gridGap = 0f)
        {
            var grid = new GameObject[rows, columns];
            var xScale = prefab.transform.localScale.x;
            var yScale = prefab.transform.localScale.z;
            
            var startX = -(columns / 2)*xScale;
            var startY = gridGap * direction * yScale;
            
            for (var row = 0; row < rows; row++)
            {
                for (var col = 0; col < columns; col++)
                {
                    Quaternion rot = direction < 0 ? Quaternion.identity : Quaternion.Euler(0f, 180f, 0f);
                    grid[row, col] =
                        Object.Instantiate(prefab, new Vector3(startX + col*xScale, 0, startY + (direction * row*yScale)), rot);
                }
            }

            return grid;
        }
    }
}