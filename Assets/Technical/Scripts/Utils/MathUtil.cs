﻿namespace Technical.Scripts.Utils
{
    public static class MathUtil
    {
        public static float ConvertValueFromOldRangeToNewRange(float value, float oldMin, float oldMax, float newMin,
            float newMax)
        {
            float oldRange = oldMax - oldMin;
            float newRange = newMax - newMin;
            if (oldRange == 0) return newMin;
            return (((value - oldMin) * newRange) / oldRange) + newMin;
        }
    }
}