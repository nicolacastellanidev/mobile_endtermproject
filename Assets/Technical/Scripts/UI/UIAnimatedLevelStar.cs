﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    public class UIAnimatedLevelStar : MonoBehaviour
    {
        public bool isActive = false;
        [SerializeField] private Image nonActiveStar;
        [SerializeField] private Image activeStar;

        private Sequence seq;

        public void Remove()
        {
            if (!isActive) return;
            isActive = false;
            
            seq.Pause();
            seq = DOTween.Sequence();
            seq.Append(activeStar.transform.DOScale(Vector3.one * 2, 0.6f))
                .Append(activeStar.DOFade(0f, 0.4f));
        }

        public void Add()
        {
            if (isActive) return;
            isActive = true;
            
            var originalColor = activeStar.color;
            originalColor.a = 1f;
            activeStar.color = originalColor;
            activeStar.transform.localScale = Vector3.one;

            seq.Pause();
            seq = DOTween.Sequence();
            seq.Append(activeStar.transform.DOScale(Vector3.one * 2, 0.1f))
                .Append(activeStar.transform.DOScale(Vector3.one, 0.2f).SetDelay(0.2f));
        }
    }
}