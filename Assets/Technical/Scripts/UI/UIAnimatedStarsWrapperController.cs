﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Technical.Scripts.UI
{
    public class UIAnimatedStarsWrapperController : MonoBehaviour
    {
        private readonly List<UIAnimatedLevelStar> stars = new List<UIAnimatedLevelStar>();

        private Coroutine animatedStarUpdateTimedCoroutine = null;
        private void Awake()
        {
            stars.AddRange(GetComponentsInChildren<UIAnimatedLevelStar>());
        }

        public void UpdateStarsAnim(int levelScore)
        {
            if (stars.Count == 0) return;
            UpdateStarAnimRoutine(levelScore);
        }

        private void UpdateStarAnimRoutine(int levelScore)
        {
            for (var i = 0; i < levelScore; i++)
            {
                UpdateStarAtIndexAnim(i, true);
            }
            
            for (var i = levelScore; i < stars.Count; i++)
            {
                UpdateStarAtIndexAnim(i, false);
            }
        }
        
        public void UpdateStarsAnimTimed(int levelScore)
        {
            if (stars.Count == 0) return;
            if(animatedStarUpdateTimedCoroutine != null)
                StopCoroutine(animatedStarUpdateTimedCoroutine);
            animatedStarUpdateTimedCoroutine = StartCoroutine(UpdateStarsAnimTimedRoutine(levelScore));
        }

        private IEnumerator UpdateStarsAnimTimedRoutine(int levelScore)
        {
            var updateStarTimer = new WaitForSeconds(0.5f);
            for (var i = 0; i < levelScore; i++)
            {
                yield return updateStarTimer;
                UpdateStarAtIndexAnim(i, true);
            }
            
            for (var i = levelScore; i < stars.Count; i++)
            {
                yield return updateStarTimer;
                UpdateStarAtIndexAnim(i, false);
            }
        }
        
        private void UpdateStarAtIndexAnim(int index, bool value)
        {
            if (stars.Count == 0 || index >= stars.Count) return;
            if (value)
            {
                stars[index].Add();
            }
            else
            {
                stars[index].Remove();
            }
        }
    }
}