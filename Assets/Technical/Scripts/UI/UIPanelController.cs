﻿using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    public class UIPanelController : MonoBehaviour
    {
        [SerializeField] private Scrollbar scrollbar;
        public void SetActive(bool next)
        {
            if (scrollbar && !next)
            {
                scrollbar.value = 1;
            }
            gameObject.SetActive(next);
        }
    }
}
