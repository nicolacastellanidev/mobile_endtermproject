﻿using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    [RequireComponent(typeof(Text))]
    public class UiProjectData : MonoBehaviour
    {
        private Text text;
        private void Start()
        {
            text = GetComponent<Text>();

            string projectName = Application.productName;
            string projectVersion = Application.version;
            string companyName = Application.companyName;
            string engineVersion = Application.unityVersion;
            text.text =
                $"{projectName} by {companyName} | Project Version {projectVersion} | Unity Version {engineVersion}";
        }

        public void ShowProjectCredits()
        {
            Popup.Instance.builder()
                .WithTitle("MGD 2021 Mobile Programming")
                .WithMessage("Created With Passion By \n\n Nicola Castellani - VR361872 \n Nicola Cisternino - VR466072")
                .WithPositiveButton(() => {}, "Close")
                .Show();
        }
    }
}
