﻿using System;
using Technical.Scripts.Managers;
using UnityEngine;

namespace Technical.Scripts.UI
{
    
    public class UIMainMenuController : MonoBehaviour
    {
        // Panels
        [Header("Panels"), Space(10)]
        [SerializeField] private UIPanelController MainMenu;
        [SerializeField] private UIPanelController LevelSelectionMenu;
        [SerializeField] private UIPanelController SettingsMenu;
        [SerializeField] private UILoadingSliderController LoadingCanvas;
        
        public void BackHome()
        {
            OpenMainMenu();
        }
        
        public void OpenSettingsMenu()
        {
            LevelSelectionMenu.SetActive(false);
            MainMenu.SetActive(false);
            SettingsMenu.SetActive(true);
        }

        public void OpenLevelSelection()
        {
            LevelSelectionMenu.SetActive(true);
            MainMenu.SetActive(false);
            SettingsMenu.SetActive(false);
        }
        
        private void OpenMainMenu()
        {
            LevelSelectionMenu.SetActive(false);
            MainMenu.SetActive(true);
            SettingsMenu.SetActive(false);
        }
        
        public void OpenLoadingPanel(string levelName)
        {
            LevelSelectionMenu.SetActive(false);
            MainMenu.SetActive(false);
            SettingsMenu.SetActive(false);
            LoadingCanvas.SetActive(true);
            LoadingCanvas.SetLevelName(levelName);
        }

        public void LoadLevel(string levelName)
        {
            SceneTransitionManager.Instance.LoadScene(levelName);
        }
        
        public void LoadLevelWithLoader(string levelName)
        {
            OpenLoadingPanel(levelName);
            SceneTransitionManager.Instance.LoadScene(levelName, true);
        }
    }
}
