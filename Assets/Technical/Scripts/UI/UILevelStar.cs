﻿using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    [RequireComponent(typeof(Image))]
    public class UILevelStar : MonoBehaviour
    {
        [SerializeField] private Sprite nonActiveSprite;
        [SerializeField] private Sprite activeSprite;
        
        
        public void Toggle(bool active)
        {
            GetComponent<Image>().sprite = active ? activeSprite : nonActiveSprite;
        }
    }
}
