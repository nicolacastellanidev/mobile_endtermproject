﻿using System.Collections;
using Technical.Scripts.Managers;
using Technical.Scripts.Models;
using Technical.Scripts.Utils;
using UnityEngine;

namespace Technical.Scripts.UI
{
    public class UISettingsController : MonoBehaviour
    {
        // Sliders
        [Header("Sliders"), Space(10), SerializeField]
        private UISettingsSliderController Slider_CameraMinZoom;
        [SerializeField] private UISettingsSliderController Slider_CameraMaxZoom;
        [SerializeField] private UISettingsSliderController Slider_CameraSpeedZoom;
        [SerializeField] private UISettingsSliderController Slider_CameraSpeedRotation;
        [SerializeField] private UISettingsSliderController Slider_MasterVolume;
        [SerializeField] private UISettingsSliderController Slider_MusicVolume;
        [SerializeField] private UISettingsSliderController Slider_UIVolume;
        [SerializeField] private UISettingsSliderController Slider_SFXVolume;

        private PersistentData data;
        private Coroutine updateDataRoutine = null;
        private void Start()
        {            
            SetupSliders();
        }

        private void SetupSliders()
        {
            data = DataManager.Instance.GetData();
            Slider_CameraMinZoom.SetValue(data.settings.cameraSettings.minCameraZoom);
            Slider_CameraMinZoom.UnsubscribeAll();
            Slider_CameraMinZoom.SubscribeToValueChange(v =>
            {
                data.settings.cameraSettings.minCameraZoom = (int) v;
                UpdateData(data);
            });

            Slider_CameraMaxZoom.SetValue(data.settings.cameraSettings.maxCameraZoom);
            Slider_CameraMaxZoom.UnsubscribeAll();
            Slider_CameraMaxZoom.SubscribeToValueChange(v =>
            {
                data.settings.cameraSettings.maxCameraZoom = v;
                UpdateData(data);
            });
            
            Slider_CameraSpeedZoom.SetValue(data.settings.cameraSettings.cameraZoomSpeed);
            Slider_CameraSpeedZoom.UnsubscribeAll();
            Slider_CameraSpeedZoom.SubscribeToValueChange(v =>
            {
                data.settings.cameraSettings.cameraZoomSpeed = (int) v;
                UpdateData(data);
            });
            
            Slider_CameraSpeedRotation.SetValue(data.settings.cameraSettings.cameraRotationSpeed);
            Slider_CameraSpeedRotation.UnsubscribeAll();
            Slider_CameraSpeedRotation.SubscribeToValueChange(v =>
            {
                data.settings.cameraSettings.cameraRotationSpeed = (int) v;
                UpdateData(data);
            });
            
            float sliderMasterVolumeValue = MathUtil.ConvertValueFromOldRangeToNewRange(data.settings.volumeSettings.masterVolume, VolumeSettings.minVolume, VolumeSettings.maxVolume, Slider_MasterVolume.MinValue, Slider_MasterVolume.MaxValue);
            Slider_MasterVolume.SetValue(sliderMasterVolumeValue);
            Slider_MasterVolume.UnsubscribeAll();
            Slider_MasterVolume.SubscribeToValueChange(v =>
            {
                data.settings.volumeSettings.masterVolume = MathUtil.ConvertValueFromOldRangeToNewRange(v, Slider_MasterVolume.MinValue, Slider_MasterVolume.MaxValue, VolumeSettings.minVolume, VolumeSettings.maxVolume);
                UpdateData(data);
            });
            
            float sliderMusicVolumeValue = MathUtil.ConvertValueFromOldRangeToNewRange(data.settings.volumeSettings.musicVolume, VolumeSettings.minVolume, VolumeSettings.maxVolume, Slider_MusicVolume.MinValue, Slider_MusicVolume.MaxValue);
            Slider_MusicVolume.SetValue(sliderMusicVolumeValue);
            Slider_MusicVolume.UnsubscribeAll();
            Slider_MusicVolume.SubscribeToValueChange(v =>
            {
                data.settings.volumeSettings.musicVolume = MathUtil.ConvertValueFromOldRangeToNewRange(v, Slider_MusicVolume.MinValue, Slider_MusicVolume.MaxValue,VolumeSettings.minVolume, VolumeSettings.maxVolume);
                UpdateData(data);
            });
            
            float sliderSfxVolumeValue = MathUtil.ConvertValueFromOldRangeToNewRange(data.settings.volumeSettings.sfxVolume, VolumeSettings.minVolume, VolumeSettings.maxVolume, Slider_SFXVolume.MinValue, Slider_SFXVolume.MaxValue);
            Slider_SFXVolume.SetValue(sliderSfxVolumeValue);
            Slider_SFXVolume.UnsubscribeAll();
            Slider_SFXVolume.SubscribeToValueChange(v =>
            {
                data.settings.volumeSettings.sfxVolume = MathUtil.ConvertValueFromOldRangeToNewRange(v, Slider_SFXVolume.MinValue, Slider_SFXVolume.MaxValue, VolumeSettings.minVolume, VolumeSettings.maxVolume);
                UpdateData(data);
            });
            
            float sliderUiVolumeValue = MathUtil.ConvertValueFromOldRangeToNewRange(data.settings.volumeSettings.uiVolume, VolumeSettings.minVolume, VolumeSettings.maxVolume, Slider_UIVolume.MinValue, Slider_UIVolume.MaxValue);
            Slider_UIVolume.SetValue(sliderUiVolumeValue);
            Slider_UIVolume.UnsubscribeAll();
            Slider_UIVolume.SubscribeToValueChange(v =>
            {
                data.settings.volumeSettings.uiVolume = MathUtil.ConvertValueFromOldRangeToNewRange(v, Slider_UIVolume.MinValue, Slider_UIVolume.MaxValue, VolumeSettings.minVolume, VolumeSettings.maxVolume);
                UpdateData(data);
            });
        }

        private void UpdateData(PersistentData data)
        {
            if(updateDataRoutine != null)
                StopCoroutine(updateDataRoutine);
            updateDataRoutine = StartCoroutine(UpdateDataExecute(data));
        }

        private IEnumerator UpdateDataExecute(PersistentData data)
        {
            DataManager.Instance.SetData(data);
            yield return new WaitForSeconds(0.3f);
            DataManager.Instance.SaveData();
        }
        
        public void ResetSettings()
        {
            Popup.Instance
                .builder()
                .WithTitle("Reset Game Data")
                .WithMessage("Are you sure? All game data will be reset to defaults.")
                .WithPositiveButton(() =>
                {
                    DataManager.Instance.SetDataAndSave(new PersistentData());
                    PlayerPrefs.DeleteAll();
                    SetupSliders();
                }, "Yes")
                .WithNegativeButton(() => { }, "No")
                .Show();
        }
    }
}