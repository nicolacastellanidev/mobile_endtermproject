﻿using System;
using System.Collections;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
 
    public class Popup : MonoBehaviour
    {
        public static Popup Instance = null;

        [Header("Popup Components")]
        [SerializeField] private GameObject background;
        [SerializeField] private GameObject container;
        [SerializeField] private Text title;
        [SerializeField] private Text message;
        [SerializeField] private Transform contentWrapper;
        [SerializeField] private Image imageWrapper;
        [SerializeField] private Button cancelButton;
        [SerializeField] private Button confirmButton;
        [SerializeField] private Button alternativeButton;

        [Header("Popup Config")] [Tooltip("Seconds to auto-reset popup")] [SerializeField]
        private float secondsToAutoReset = 3f;

        [SerializeField] private AudioSource uiAudioSource;
        [SerializeField] private AudioSource fxAudioSource;
        
        private Action onShowAction = null;
        private void Awake()
        {
            if (!Instance)
                Instance = this;
            else if (Instance != this)
            {
                Destroy(gameObject);
            }

            uiAudioSource = GetComponent<AudioSource>();
        }

        /// <summary>
        /// Creates a Popup builder instance
        /// </summary>
        public IPopupBuilder builder()
        {
            return new Builder(this);
        }
        
        public interface IPopupBuilder
        {
            IPopupBuilder WithTitle(String text);
            IPopupBuilder WithMessage(String text);
            IPopupBuilder WithPositiveButton(UnityAction onPositiveButtonClick, String buttonText = "");
            IPopupBuilder WithNegativeButton(UnityAction onNegativeButtonClick, String buttonText = "");
            IPopupBuilder WithAlternativeButton(UnityAction onAlternativeButtonClick, String buttonText = "");
            IPopupBuilder WithContent(GameObject content);
            IPopupBuilder WithImage(Sprite image);
            IPopupBuilder AddOnShowAction(Action action);
            IPopupBuilder WithAudioClip(AudioClip clip);
            void Show();
        }
        private class Builder : IPopupBuilder
        {
            private readonly Popup m_Controller;
            public Builder(Popup controller)
            {
                m_Controller = controller;
            }

            public IPopupBuilder WithTitle(String text)
            {
                m_Controller.title.text = text;
                m_Controller.title.enabled = true;
                return this;
            }

            public IPopupBuilder WithMessage(String text)
            {
                m_Controller.message.text = text;
                m_Controller.message.enabled = true;
                return this;
            }
            
            public IPopupBuilder WithContent(GameObject content)
            {
                if (!content) return this;
                content.transform.SetParent(m_Controller.contentWrapper);
                m_Controller.contentWrapper.gameObject.SetActive(true);
                return this;
            }
            
            public IPopupBuilder WithImage(Sprite image)
            {
                if (!image) return this;
                m_Controller.imageWrapper.gameObject.SetActive(true);
                m_Controller.imageWrapper.sprite = image;
                return this;
            }

            public IPopupBuilder WithAudioClip(AudioClip clip)
            {
                m_Controller.fxAudioSource.clip = clip;
                return this;
            }

            public IPopupBuilder WithPositiveButton(UnityAction onPositiveButtonClick, String buttonText = "")
            {
                m_Controller.confirmButton.gameObject.SetActive(true);
                m_Controller.confirmButton.onClick.AddListener(m_Controller.Reset);
                m_Controller.confirmButton.onClick.AddListener(onPositiveButtonClick);
                if (!string.IsNullOrEmpty(buttonText))
                {
                    m_Controller.confirmButton.gameObject.GetComponentInChildren<Text>().text = buttonText;
                }
                return this;
            }

            public IPopupBuilder WithNegativeButton(UnityAction onNegativeButtonClick, String buttonText = "")
            {
                m_Controller.cancelButton.gameObject.SetActive(true);
                m_Controller.cancelButton.onClick.AddListener(m_Controller.Reset);
                m_Controller.cancelButton.onClick.AddListener(onNegativeButtonClick);
                if (!string.IsNullOrEmpty(buttonText))
                {
                    m_Controller.cancelButton.gameObject.GetComponentInChildren<Text>().text = buttonText;
                }
                return this;
            }

            public IPopupBuilder WithAlternativeButton(UnityAction onAlternativeButtonClick, string buttonText = "")
            {
                m_Controller.alternativeButton.gameObject.SetActive(true);
                m_Controller.alternativeButton.onClick.AddListener(onAlternativeButtonClick);
                m_Controller.alternativeButton.onClick.AddListener(m_Controller.Reset);
                if (!string.IsNullOrEmpty(buttonText))
                {
                    m_Controller.alternativeButton.gameObject.GetComponentInChildren<Text>().text = buttonText;
                }
                return this;
            }

            public IPopupBuilder AddOnShowAction(Action action)
            {
                m_Controller.onShowAction += action;
                return this;
            }
            public void Show()
            {
                if (!m_Controller.cancelButton.gameObject.activeSelf &&
                    !m_Controller.confirmButton.gameObject.activeSelf)
                {
                    m_Controller.AutoReset();
                }
                m_Controller.background.SetActive(true);
                m_Controller.PlayPopupAnimationEnter();
                m_Controller.fxAudioSource.Play();
            } 
        }


        /// <summary>
        /// Auto hide popup
        /// </summary>
        private void AutoReset()
        {
            StartCoroutine(Reset(secondsToAutoReset));
        }
        
        /// <summary>
        /// Hides popup after seconds
        /// <param name="seconds">Seconds to wait to hide popup</param>
        /// </summary>
        private IEnumerator Reset(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            Reset();
        }
        /// <summary>
        /// Hides popup
        /// </summary>
        private void Reset()
        {
            title.text = "";
            title.enabled = false;
            
            message.text = "";
            message.enabled = false;
            
            cancelButton.onClick.RemoveAllListeners();
            cancelButton.gameObject.SetActive(false);
            
            confirmButton.onClick.RemoveAllListeners();
            confirmButton.gameObject.SetActive(false);
            
            background.SetActive(false);
            
            contentWrapper.gameObject.SetActive(false);
            
            imageWrapper.gameObject.SetActive(false);
            imageWrapper.sprite = null;

            onShowAction = null;

            fxAudioSource.clip = null;
            uiAudioSource.clip = null;
            
            for(int i = 0; i < contentWrapper.childCount; ++i)
                Destroy(contentWrapper.GetChild(i).gameObject);
        }

        private void PlayPopupAnimationEnter()
        {
            var sequence = DOTween.Sequence();
            sequence
                .Append(container.transform.DOScale(Vector3.one * 0.5f, 0.65f).From().SetEase(Ease.OutBounce))
                .AppendCallback(() => { onShowAction?.Invoke(); })
                .Play();
        }
        /// <summary>
        /// Handles interactable prop of cancel button
        /// </summary>
        /// <param name="active">Set interactable or not</param>
        private void ToggleCancel(bool active)
        {
            cancelButton.interactable = active;
        }
        
        /// <summary>
        /// Handles interactable prop of cancel button
        /// </summary>
        /// <param name="active">Set interactable or not</param>
        private void ToggleConfirm(bool active)
        {
            confirmButton.interactable = active;
        }
        
        /// <summary>
        /// Handles interactable prop of all buttons
        /// </summary>
        /// <param name="active">Set interactable or not</param>
        private void ToggleAllButtons(bool active)
        {
            confirmButton.interactable = active;
            cancelButton.interactable = active;
        }
    }
}