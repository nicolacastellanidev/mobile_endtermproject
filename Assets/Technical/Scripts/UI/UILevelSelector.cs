﻿using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    public class UILevelSelector : MonoBehaviour
    {
        private UIStarsWrapperController starsController;

        private void Awake()
        {
            starsController = GetComponentInChildren<UIStarsWrapperController>();
        }

        public void Set(LevelData level)
        {
            GetComponent<Button>().interactable = level.accessible;
            starsController.UpdateStars(level.score);
        }
        
    }
}
