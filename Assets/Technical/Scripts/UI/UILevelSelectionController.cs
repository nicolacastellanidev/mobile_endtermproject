﻿using System;
using System.Collections.Generic;
using Technical.Scripts.Managers;
using Technical.Scripts.Models;
using UnityEngine;

namespace Technical.Scripts.UI
{
    public class UILevelSelectionController : MonoBehaviour
    {
        private List<UILevelSelector> levelSelectors = new List<UILevelSelector>();

        private void Awake()
        {
            levelSelectors.AddRange(GetComponentsInChildren<UILevelSelector>());
        }

        private void Start()
        {
            DataManager.Instance.DataUpdated += OnGameDataUpdate;
            UpdateLevelSelectors(DataManager.Instance.GetData());
        }

        private void OnDestroy()
        {
            DataManager.Instance.DataUpdated -= OnGameDataUpdate;
        }

        private void OnGameDataUpdate(PersistentData data)
        {
            UpdateLevelSelectors(data);
        }

        private void UpdateLevelSelectors(PersistentData data)
        {
            var levels = data.gameData.levels;
            for (var i = 0; i < levelSelectors.Count; i++)
            {
                if (i > levels.Length - 1) return;
                levelSelectors[i].Set(levels[i]);
            }
        }
    }
}
