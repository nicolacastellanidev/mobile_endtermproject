﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Technical.Scripts.UI
{
    public class UILoadingSliderController : MonoBehaviour
    {
        [SerializeField] private GameObject wrapper;
        [SerializeField] private string[] sentences;
        [SerializeField] private Text sentenceText;
        [SerializeField] private Text levelNameText;
        private int lastRandomSentenceIndex = 0;
        private Animator _animator;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        private void Start()
        {
            wrapper.SetActive(false);
        }

        public void SetActive(bool next)
        {
            wrapper.SetActive(next);

            if (next)
            {
                _animator.Play("slider_default", -1, 0f);
                StartCoroutine(SetRandomSentence());
            }
            else
            {
                StopAllCoroutines();
            }
        }
        
        private IEnumerator SetRandomSentence()
        {
            while (true)
            {
                var currentRandomSentenceIndex = Random.Range(0, sentences.Length);
                while (currentRandomSentenceIndex == lastRandomSentenceIndex)
                {
                    currentRandomSentenceIndex = Random.Range(0, sentences.Length);
                }

                lastRandomSentenceIndex = currentRandomSentenceIndex;
                sentenceText.text = sentences[currentRandomSentenceIndex];
                yield return new WaitForSeconds(5f);
            }
        }

        public void SetLevelName(string levelName)
        {
            levelNameText.text = levelName;
        }
    }
}
