﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
   public class UISettingsSliderController : MonoBehaviour
   {
      [SerializeField] private Slider slider;
      [SerializeField] private Text valueLabel;

      private void Start()
      {
         slider.onValueChanged.AddListener(UpdateText);
         UpdateText(slider.value);
      }

      public void UnsubscribeAll()
      {
         slider.onValueChanged.RemoveAllListeners();
         slider.onValueChanged.AddListener(UpdateText);
      }
      
      public void SubscribeToValueChange(UnityAction<float> call)
      {
         slider.onValueChanged.AddListener(call);
      }

      public void SetValue(float value)
      {
         slider.value = value;
         UpdateText(value);
      }

      public float MinValue => slider.minValue;
      public float MaxValue => slider.maxValue;
      

      private void UpdateText(float value)
      {
         valueLabel.text = $"{value}";
      }
   }
}
