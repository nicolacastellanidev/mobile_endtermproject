﻿using System.Collections.Generic;
using UnityEngine;

namespace Technical.Scripts.UI
{
    public class UIStarsWrapperController : MonoBehaviour
    {
        private readonly List<UILevelStar> stars = new List<UILevelStar>();

        private void Awake()
        {
            stars.AddRange(GetComponentsInChildren<UILevelStar>());
        }
        
        public void UpdateStars(int levelScore)
        {
            if (stars.Count == 0) return;
            for (var i = 0; i < levelScore; i++)
            {
                stars[i].Toggle(true);
            }
            
            for (var i = levelScore; i < stars.Count; i++)
            {
                stars[i].Toggle(false);
            }
        }
    }
}
