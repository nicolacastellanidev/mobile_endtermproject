﻿using System;
using System.Collections;
using JetBrains.Annotations;
using Technical.Scripts.GameMode;
using Technical.Scripts.GameMode.SimulationMode;
using Technical.Scripts.Managers;
using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

namespace Technical.Scripts.UI
{
    public class SimulationModeUIController : MonoBehaviour
    {
        [Space(10), Header("Panels")]
        [SerializeField] private GameObject bottomPanel;

        [Space(10), Header("Texts")]
        [SerializeField] private Text unitCounterText;
        [SerializeField] private Text timerText;
        
        [Space(10), Header("Buttons")]
        [SerializeField] private Button meleeSelector;
        [SerializeField] private Button archerSelector;
        [SerializeField] private Button priestSelector;
        [SerializeField] private Button heroSelector;
        [SerializeField] private Button ballistaSelector;
        [SerializeField] private Button clearCellButton;
        [SerializeField] private Button startGameButton;
        [SerializeField] private Button pauseGameButton;
        [SerializeField] private Button closePanelButton;

        [Space(10), Header("Images")]
        [SerializeField] private Image cameraOverlayImage;
        
        [Space(10), Header("Others")]
        [SerializeField] private UIAnimatedStarsWrapperController starsWrapper;
        
        private GridCellController _cellSelected;

        #region Lifecycle
        private void Start()
        {
            InitUI();
            StartCoroutine(FadeCameraOverlayTo(0.5f));
        }
        
        #endregion
        
        #region Public Interface
        public void CellSelected(GridCellController cell)
        {
            bottomPanel.SetActive(cell != null);
            _cellSelected = cell;
        }
        
        public void DeselectSelectedCell()
        {
            bottomPanel.SetActive(false);
            _cellSelected.Deselect();
            _cellSelected = null;
        }
        
        #endregion
        
        #region Private
        private void OnScoreUpdate(int score)
        {
            starsWrapper.UpdateStarsAnim(score);
        }

        private void OnGameStart()
        {
            StartCoroutine(FadeCameraOverlayTo(0f));
        }

        private void OnGameEnd()
        {
            StartCoroutine(FadeCameraOverlayTo(0.5f));
        }
        
        private IEnumerator FadeCameraOverlayTo(float to)
        {
            if (!cameraOverlayImage)
            {
                yield return null;
            }
            else
            {
                var nextColor = cameraOverlayImage.color;
                while (Math.Abs(nextColor.a - to) > 0.01f)
                {
                    nextColor.a = Mathf.Lerp(nextColor.a, to, 0.1f);
                    cameraOverlayImage.color = nextColor;
                    yield return new WaitForSeconds(0.05f);
                }
            }
        }

        private void OnTimeUpdate(float amount)
        {
            var minutes = (int) amount / 60;
            var seconds = (int) amount % 60;
            timerText.text = $"{minutes}:{(seconds < 10 ? $"0{seconds}" : seconds.ToString())}";
        }

        private void OnDestroy()
        {
            GameModeController.Instance.onPlayerUnitUpdate -= UpdateButtons;
        }

        private void InitUI()
        {
            var settings = GameModeController.Instance.Settings;
            unitCounterText.text = $"0/{settings.maxSpawnableUnits}";

            meleeSelector.onClick.AddListener( () => SpawnUnit(GameplayObjectModel.Melee));
            archerSelector.onClick.AddListener( () => SpawnUnit(GameplayObjectModel.Archer));
            priestSelector.onClick.AddListener( () => SpawnUnit(GameplayObjectModel.Priest));
            heroSelector.onClick.AddListener( () => SpawnUnit(GameplayObjectModel.Hero));
            ballistaSelector.onClick.AddListener( () => SpawnUnit(GameplayObjectModel.Ballista));
            clearCellButton.onClick.AddListener( ClearSelectedCell );
            startGameButton.onClick.AddListener( StartGame );
            pauseGameButton.onClick.AddListener( PauseGame );
            closePanelButton.onClick.AddListener( DeselectSelectedCell );
            
            GameModeController.Instance.onPlayerUnitUpdate += UpdateButtons;
            
            GameModeController.Instance.onTimeUpdate += OnTimeUpdate;
            OnTimeUpdate(GameModeController.Instance.gameDurationInSeconds);
            GameModeController.Instance.onGameStart += OnGameStart;
            GameModeController.Instance.onGameEnd += OnGameEnd;
            
            if (ScoreManager.Instance)
            {
                ScoreManager.Instance.onScoreUpdate += OnScoreUpdate;
                OnScoreUpdate(ScoreManager.Instance.GetScore());
            }
            
            bottomPanel.SetActive(false);
        }
        
        private void PauseGame()
        {
            GameModeController.Instance.Pause();
        }

        private void ClearSelectedCell()
        {
            if (_cellSelected.gameObject)
            {
                _cellSelected.SetGameplayObjectController(null);
            }
        }
        
        private void SpawnUnit(GameplayObjectModel model)
        {
            if (!_cellSelected) return;
            _cellSelected.SetGameplayObjectController(
                    GameModeController.Instance.SpawnUnit(model, Team.PLAYER, _cellSelected.transform.position, _cellSelected.transform.rotation)
            );
        }

        private void UpdateButtons(Player player)
        {
            if (player.Team != Team.PLAYER) return;
            var settings = GameModeController.Instance.Settings;
            var occupiedUnitSpace = player.unitOccupation;
            
            meleeSelector.interactable = settings.maxSpawnableUnits - occupiedUnitSpace >= settings.playerUnitSettings.meleeSettings.cost;
            archerSelector.interactable = settings.maxSpawnableUnits - occupiedUnitSpace >= settings.playerUnitSettings.archerSettings.cost;
            priestSelector.interactable = settings.maxSpawnableUnits - occupiedUnitSpace >= settings.playerUnitSettings.priestSettings.cost;
            heroSelector.interactable = settings.maxSpawnableUnits - occupiedUnitSpace >= settings.playerUnitSettings.heroSettings.cost;
            ballistaSelector.interactable = settings.maxSpawnableUnits - occupiedUnitSpace >= settings.playerUnitSettings.ballistaSettings.cost;
            
            unitCounterText.text = $"{occupiedUnitSpace}/{settings.maxSpawnableUnits}";
        }

        private void StartGame()
        {
            GameModeController.Instance.StartGame();
        }
        
        #endregion
    }
}
