﻿using Technical.Scripts.Controllers.Base;
using UnityEngine;

namespace Technical.Scripts.Controllers.Structures
{
    public class StructureSearchController : BaseSearchController
    {
        protected void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(transform.position, Vector3.one * data.range);
        }
    }
}