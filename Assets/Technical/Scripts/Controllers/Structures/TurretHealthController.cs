﻿using Technical.Scripts.Controllers.Base;

namespace Technical.Scripts.Controllers.Structures
{
    public class TurretHealthController : BaseHealthController
    {
        protected override void Die()
        {
            base.Die();
            if (data.dieEffect) Instantiate(data.dieEffect, transform.position, transform.rotation);
        }

        protected override void DestroyMe()
        {
            Destroy(gameObject);
        }
    }
}