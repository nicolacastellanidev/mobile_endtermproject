﻿using System.Linq;
using UnityEngine;

namespace Technical.Scripts.Controllers.Structures
{
    public class StructureSelectTargetBehavior : SelectTargetBehavior
    {
        public override GameObject SelectTarget(Collider[] potentialTargets, GameObject currentTarget, GameObject searcher)
        {
            return potentialTargets
                .Where(c => c != null)
                .OrderBy(c => (c.transform.position - searcher.transform.position).sqrMagnitude)
                .FirstOrDefault()?.gameObject;
        }
    }
    
    public class StructureFindTargetBehavior : FindTargetBehavior
    {
        public override Collider[] FindNearbyTargets(Vector3 position, float range, LayerMask whatIsTarget)
        {
            Collider[] results = new Collider[10];
            var halfSize = Vector3.one * (range / 2f);
            Physics.OverlapBoxNonAlloc(position, halfSize, results, Quaternion.identity, whatIsTarget);
            return results;
        }
    }
}