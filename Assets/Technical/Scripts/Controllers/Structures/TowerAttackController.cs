﻿using Technical.Scripts.Commands;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Models;
using UnityEngine;

namespace Technical.Scripts.Controllers.Structures
{
    public class TowerAttackController : BaseInteractionController
    { 
        [SerializeField] private RangedInteraction interactionData;

        protected override void Awake()
        {
            base.Awake();
            interactionCommand =
                new SingleRangedAttackCommand(interactionData.bullet, interactionData.bulletSpawnPoint, gameObject);
            SetBaseInteractionData(interactionData.baseInteraction);
        }

        protected override void ExecuteInteraction()
        {
            audioHandler?.PlayAudioAction(AudioActionType.Attack);
            interactionCommand.Execute(target.transform);
        }

        protected override bool IsTargetInRange()
        {
            return target != null; //standard towers can immediately attack a target in sight
        }

        protected override void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position, Vector3.one * interactionData.baseInteraction.range); //data.range is equals to search range
        }
    }
}