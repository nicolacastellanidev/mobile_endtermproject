﻿using System;
using Technical.Scripts.Controllers.Base;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Controllers.Structures
{
    public class StructureTargetOrchestrator: DisableOnGameplayObjectDiedBehaviour
    {
        private GameObject mCurrentTarget;
        [Serializable] public class TargetUpdateEvent : UnityEvent<GameObject> { }

        public TargetUpdateEvent onTargetUpdate;

        private bool _isActive = false;

        public void OnStatusChange(UnitStatus status)
        {
            _isActive = status == UnitStatus.ACTIVE;
            onTargetUpdate?.Invoke(_isActive ? mCurrentTarget : null);
        }

        public void SetTarget(GameObject nextTarget)
        {
            if (nextTarget == mCurrentTarget) return;
            mCurrentTarget?.GetComponent<BaseHealthController>().onGameplayObjectDead.RemoveListener(OnTargetDied);
            mCurrentTarget = nextTarget;
            if (!_isActive) return;
            onTargetUpdate?.Invoke(mCurrentTarget);
            if (mCurrentTarget == null) return;
            mCurrentTarget.GetComponent<BaseHealthController>().onGameplayObjectDead.AddListener(OnTargetDied);
        }

        private void OnTargetDied(GameObject _target)
        {
            print($"{_target.name} is dead, should get new target.");
            SetTarget(null);
        }
    }
}