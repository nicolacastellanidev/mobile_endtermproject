﻿using System;
using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Controllers.Spells
{
    public class AoeSpellSearchController : MonoBehaviour
    {
        [SerializeField]
        protected SpellSearch data;
        [Serializable] public class TargetsUpdateEvent : UnityEvent<GameObject[]> {}
        public TargetsUpdateEvent onTargetsUpdate;
        
        private GameObject[] targets = null;
        private SelectTargetsBehavior selectTargetsBehavior = null;
        private FindTargetBehavior findTargetBehavior = null;

        private float mCurrentRefreshCooldown = 0f;

        protected virtual void Awake()
        {
            selectTargetsBehavior = SelectTargetsBehavior.Create(data.selectTargetsBehavior);
            findTargetBehavior = FindTargetBehavior.Create(data.findTargetBehavior);
        }
        
        private void Update()
        {
            mCurrentRefreshCooldown -= Time.deltaTime;
            if (mCurrentRefreshCooldown > 0f) return;
            Search();
            mCurrentRefreshCooldown = data.refreshRate;
        }

        protected virtual void Search()
        {
            if (targets?.Length > 0 || selectTargetsBehavior == null) return;
            var potentialTargets =
                findTargetBehavior.FindNearbyTargets(transform.position, data.range, data.whatIsTarget);
            var updatedTargets = potentialTargets.Length > 0
                ? selectTargetsBehavior.SelectTargets(potentialTargets, gameObject)
                : null; //default init to null, to handle zero potential targets

            targets = updatedTargets;
            onTargetsUpdate?.Invoke(targets);
        }

        /// <summary>
        /// Updates target from other GPO (e.g. special spells)
        /// </summary>
        /// <param name="whatIsTarget"></param>
        public void SetTarget(LayerMask whatIsTarget)
        {
            data.whatIsTarget = whatIsTarget;
        }
        
        protected void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, data.range);
        }
    }
}