﻿using Technical.Scripts.Controllers.Base;
using UnityEngine;

namespace Technical.Scripts.Controllers.Spells
{
    /// <summary>
    /// Attack controller AOE Spells
    /// </summary>
    public class AoeSpellAttackController : BaseSpellInteractionController
    {
        protected override void ExecuteInteraction()
        {
            data.spellToExecute.Execute(targets, data.amount);
        }
        
        public void OnTargetsUpdate(GameObject[] newTargets)
        {
            targets = newTargets;
            ExecuteInteraction();
        }
    }
}