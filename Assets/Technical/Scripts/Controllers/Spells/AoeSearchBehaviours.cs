﻿using System.Linq;
using UnityEngine;

namespace Technical.Scripts.Controllers.Spells
{
    public class AoeSelectTargetsBehavior : SelectTargetsBehavior
    {
        public override GameObject[] SelectTargets(Collider[] potentialTargets, GameObject searcher)
        {
            return (from t in potentialTargets where t?.gameObject select t.gameObject).ToArray();
        }
    }
    
    public class AoeFindTargetBehavior : FindTargetBehavior
    {
        public override Collider[] FindNearbyTargets(Vector3 position, float range, LayerMask whatIsTarget)
        {
            var colliders = new Collider[10];
            Physics.OverlapSphereNonAlloc(position, range, colliders, whatIsTarget);
            return colliders;
        }
    }
}