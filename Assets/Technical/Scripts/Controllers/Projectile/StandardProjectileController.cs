﻿using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Models;
using UnityEngine;
using Utils;

namespace Technical.Scripts.Controllers.Projectile
{
    public class StandardProjectileController : BaseProjectileController
    {
        [SerializeField] private float damage;
        [SerializeField] private GameObject hitEffect;
        private void Start()
        {
            GetComponent<TrajectoryController>().Launch(targetPos);   
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.layer == whatIsEnemy)
            {
                if(hitEffect)
                    Instantiate(hitEffect, other.contacts[0].point, transform.rotation);
                other.gameObject.GetComponent<BaseHealthController>()?.Hit(damage);
            }
            
            Destroy(gameObject);
        }
    }
}