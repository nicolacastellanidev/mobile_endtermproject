﻿using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Controllers.Spells;
using UnityEngine;

namespace Technical.Scripts.Controllers.Projectile
{
    public class BallistaProjectileController : BaseProjectileController
    {
        [SerializeField] private GameObject hitEffectSpell;
        private void Start()
        {
            GetComponent<TrajectoryController>().Launch(targetPos);   
        }

        private void OnCollisionEnter(Collision other)
        {
            var go = Instantiate(hitEffectSpell, other.GetContact(0).point, Quaternion.identity);
            go.GetComponent<AoeSpellSearchController>()?.SetTarget(1 << whatIsEnemy);
            Destroy(gameObject);
        }
    }
}