﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Technical.Scripts.Controllers
{
    public enum AudioActionType
    {
        Attack,
        Death,
        Engage,
        Auto
    }

    [Serializable]
    public struct AudioAction
    {
        public AudioActionType type;
        public AudioClip clip;
    }
    [RequireComponent(typeof(AudioSource))]
    public class AudioHandler : MonoBehaviour
    {
        [SerializeField] private List<AudioAction> actions = new List<AudioAction>();
        public Action<AudioActionType> onAudioActionEnd;
        
        private AudioSource source;
        private void Awake()
        {
            source = GetComponent<AudioSource>();
        }

        private void Start()
        {
            PlayAudioAction(AudioActionType.Auto);
        }

        public void PlayAudioAction(AudioActionType type)
        {
            if (actions.Count == 0) return;
            var clipsWithTheDesiredType = actions.Where(audioAction => audioAction.type == type).ToList();
            
            if (clipsWithTheDesiredType.Count == 0) return;
            
            int clipToPlayIndex = Random.Range(0, clipsWithTheDesiredType.Count);
            var actionType = clipsWithTheDesiredType[clipToPlayIndex].type;
            var clip = clipsWithTheDesiredType[clipToPlayIndex].clip;
            
            if (clip)
            {
                source.PlayOneShot(clip);
            }

            StartCoroutine(OnClipFinished(clip.length, actionType));
        }

        private IEnumerator OnClipFinished(float time, AudioActionType type)
        {
            yield return new WaitForSeconds(time);
            onAudioActionEnd?.Invoke(type);
        }
    }
}
