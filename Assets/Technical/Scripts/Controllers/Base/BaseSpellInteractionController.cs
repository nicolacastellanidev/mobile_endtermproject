﻿using Technical.Scripts.Models;
using UnityEngine;

namespace Technical.Scripts.Controllers.Base
{
    public abstract class BaseSpellInteractionController: MonoBehaviour
    {
        protected GameObject[] targets = null;
        [SerializeField]
        protected SpellInteraction data;

        public void Start()
        {
            ExecuteInteraction();
        }
        protected virtual void ExecuteInteraction() {}
    }
}