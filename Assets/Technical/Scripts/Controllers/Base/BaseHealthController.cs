﻿using System;
using Technical.Scripts.Controllers.Units;
using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Controllers.Base
{
    /// <summary>
    /// Base health controller. Handles damage and die.
    /// </summary>
    public class BaseHealthController : MonoBehaviour
    {
        // model management
        [SerializeField]
        protected Health data;

        // controller variables
        private UnitAnimationController animator;
        private AudioHandler audioHandler;

        private float maxLife = 0f; // original life
        
        // behaviour commands
        [Serializable]
        public class GameplayObjectDiedEvent : UnityEvent<GameObject> {}
        public GameplayObjectDiedEvent onGameplayObjectDead;
        
        [Serializable]
        public class GameplayObjectHitEvent : UnityEvent<float> {}
        public GameplayObjectHitEvent onHit;

        private bool isDead = false;
        
        protected void Awake()
        {
            maxLife = data.life;
            animator = GetComponent<UnitAnimationController>();
            audioHandler = GetComponent<AudioHandler>();
        }

        private void Start()
        {
            onHit?.Invoke(maxLife);
        }

        /// <summary>
        /// Called when controller IsDead()
        /// </summary>
        protected virtual void Die()
        {
            if (isDead) return;
            isDead = true;
            
            if (animator)
            {
                animator.Animate(UnitAnimationController.Die);
                animator.enabled = false;
            }

            if (audioHandler)
            {
                audioHandler.PlayAudioAction(AudioActionType.Death);
                audioHandler.enabled = false;
            }

            onGameplayObjectDead?.Invoke(gameObject);
            Invoke(nameof(DestroyMe), data.dieAfter);
        }

        /// <summary>
        /// Destroy gameobject
        /// </summary>
        protected virtual void DestroyMe()
        {
            if (data.dieEffect) Instantiate(data.dieEffect, transform.position, transform.rotation);
            Destroy(gameObject);
        }

        /// <summary>
        /// Check if current life is greater than 0.
        /// </summary>
        /// <returns>bool</returns>
        public bool IsDead()
        {
            return data.life <= 0;
        }

        /// <summary>
        /// Called when entity is hitted
        /// </summary>
        /// <param name="damage">float</param>
        public void Hit(float damage)
        {
            data.life -= damage;
            onHit?.Invoke(data.life);
            if (IsDead())
            {
                Die();
            }
        }
        
        /// <summary>
        /// Increase entity life by amount. Doesn't exceed maxLife
        /// </summary>
        /// <param name="amount">float</param>
        public virtual void Heal(float amount)
        {
            if (isDead) return;
            data.life = Math.Min(maxLife, data.life + amount);
            onHit?.Invoke(data.life);
        }

        /// <summary>
        /// Check if current life is equal to maxLife
        /// </summary>
        /// <returns></returns>
        public virtual bool IsMaxHP()
        {
            return data.life >= maxLife;
        }
        
        /// <summary>
        /// Returns a value in the interval [0, 100] stating a percentage of remaining health
        /// </summary>
        /// <returns></returns>
        public float GetHealthRemainingPercentage()
        {
            if (maxLife == 0) return 0f;
            return data.life / maxLife;
        }
    }
}