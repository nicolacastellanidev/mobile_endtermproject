﻿using UnityEngine;

namespace Technical.Scripts.Controllers.Base
{
    public abstract class BaseProjectileController : MonoBehaviour
    {
        protected LayerMask whatIsEnemy = 0;
        protected Vector3 targetPos;

        public void Init(Vector3 targetPos, GameObject parent, LayerMask whatIsEnemy)
        {
            if (parent.layer == LayerMask.NameToLayer("Dead"))
            {
                Destroy(gameObject);
                return;
            }
            
            var targetLayer = LayerMask.NameToLayer(LayerMask.LayerToName(parent.layer) + "_Projectile");
            this.whatIsEnemy = whatIsEnemy;
            this.targetPos = targetPos;
            gameObject.layer = targetLayer;
        }
    }
}