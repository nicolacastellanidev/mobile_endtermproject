﻿using System;
using Technical.Scripts.Commands.Base;
using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Controllers.Base
{
    public abstract class BaseInteractionController : DisableOnGameplayObjectDiedBehaviour
    {
        #region Data Members
        private Interaction data = null;

        private float cooldown = 0f;
        
        protected GameObject target = null;
        protected BaseAnimationController animator;
        protected AudioHandler audioHandler;

        protected SingleTargetCommand interactionCommand;
        #endregion
        
        #region Events
        [Serializable]
        public class TargetRangeUpdateEvent : UnityEvent<bool>{}
        public TargetRangeUpdateEvent onTargetRangeUpdate;
        #endregion
        
        protected virtual void Awake()
        {
            animator = GetComponent<BaseAnimationController>();
            audioHandler = GetComponent<AudioHandler>();
        }
        
        protected virtual void Update()
        {
            if (data == null || !target) return;

            cooldown -= Time.deltaTime;
            if (cooldown > 0) return;
            
            var isInteractionAvailable = IsTargetInRange();
            onTargetRangeUpdate?.Invoke(isInteractionAvailable);

            if (!isInteractionAvailable) return;

            Interact();
        }
        
        protected abstract void ExecuteInteraction();
        
        protected virtual bool IsTargetInRange()
        {
            if (data == null) return false;
            var dist = Vector3.Distance(transform.position, target.transform.position);
            return dist <= data.range;
        }

        protected void SetBaseInteractionData(Interaction baseInteraction)
        {
            data = baseInteraction;
        }
        
        private void Interact()
        {
            if (cooldown > 0f) return;
            cooldown = data.ratio;
            ExecuteInteraction();
        }

        public void OnTargetUpdate(GameObject newTarget)
        {
            target = newTarget;
        }
        
        protected virtual void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            if(target) Gizmos.DrawLine(transform.position, target.transform.position);
        }
    }
}