﻿using Technical.Scripts.Controllers.Units;
using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace Technical.Scripts.Controllers.Base
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class BaseMoveController : DisableOnGameplayObjectDiedBehaviour
    {
        [SerializeField]
        protected Move data;

        private NavMeshAgent agent;
        private Vector3? destination;

        private BaseAnimationController animator;
   
        protected void Awake()
        {
            agent = GetComponent<NavMeshAgent>();
            agent.speed = data.speed;
            animator = GetComponent<BaseAnimationController>();
        }

        private void Update()
        {
            if (destination == null) return;
            //se la distanza tra me e il mio bersaglio è più alta del delta continuo
            if (Vector3.Distance(transform.position, (Vector3) destination) > data.deltaDistanceToReachDestination) return;
            // altrimenti Stop
            Stop();
        }

        private void OnDestroy()
        {
            Stop();
        }

        public void SetDestination(Vector3? nextDestination)
        {
            destination = nextDestination;
            if (destination != null) Move(); else Stop();
        }
        
        private void Move()
        {
            agent.isStopped = false;
            if (destination == null) return;
            agent.SetDestination((Vector3) destination);
            animator?.Animate(UnitAnimationController.Move);
        }
        
        private void Stop()
        {
            if(!agent.isActiveAndEnabled) return;
            agent.isStopped = true;
            agent.velocity = Vector3.zero;
            animator?.Animate(UnitAnimationController.Idle);
        }
    }
}