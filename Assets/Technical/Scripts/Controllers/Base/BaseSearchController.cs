﻿using System;
using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Controllers.Base
{
    public abstract class BaseSearchController : DisableOnGameplayObjectDiedBehaviour
    {
        #region Data Members
        
        [SerializeField]
        protected Search data;

        private float mCurrentRefreshCooldown = 0f;
        private GameObject target = null;
        
        private SelectTargetBehavior selectTargetBehavior = null;
        private FindTargetBehavior findTargetBehavior = null;
        #endregion
        
        #region Events
        [Serializable] public class TargetUpdateEvent : UnityEvent<GameObject> {}
        public TargetUpdateEvent onTargetUpdate;
        #endregion

        #region LifeCycle

        protected virtual void Awake()
        {
            selectTargetBehavior = SelectTargetBehavior.Create(data.selectTargetBehavior);
            findTargetBehavior = FindTargetBehavior.Create(data.findTargetBehavior);
        }

        private void Update()
        {
            mCurrentRefreshCooldown -= Time.deltaTime;
            if (mCurrentRefreshCooldown > 0f) return;
            Search();
            mCurrentRefreshCooldown = data.refreshRate;
        }

        #endregion
        
        protected virtual void Search()
        {
            if (selectTargetBehavior == null || findTargetBehavior == null) return;
            var potentialTargets = findTargetBehavior.FindNearbyTargets(transform.position, data.range, data.whatIsTarget);
            var updatedTarget = potentialTargets.Length > 0 
                ? selectTargetBehavior.SelectTarget(potentialTargets, target, gameObject)
                : null; //default init to null, to handle zero potential targets

            if (updatedTarget == target) return;
            target = updatedTarget;
            onTargetUpdate?.Invoke(target);
        }
    }
}