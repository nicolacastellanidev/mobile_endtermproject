﻿using UnityEngine;

namespace Technical.Scripts.Controllers.Base
{
    /// <summary>
    /// Base animation controller - handles Animator current state
    /// </summary>
    public abstract class BaseAnimationController: MonoBehaviour
    {
        [SerializeField] protected Animator animator;
        private string currentAnimation;
        protected virtual void Awake()
        {
            if (animator) return;
            throw new MissingReferenceException("Animator is missing from component");
        }
        /// <summary>
        /// Changes animator state, if current state is equal to next, returns.
        /// Stops animator if next animation is null.
        /// </summary>
        /// <param name="animationName"></param>
        public virtual void Animate(string animationName, float speed = 1f)
        {
            if(!enabled) return;
            animator.speed = speed;
            if (animationName == null)
            {
                animator.StopPlayback();
                currentAnimation = null;
                return;
            }
            
            if (animationName == currentAnimation) return;
            
            currentAnimation = animationName;
            animator.Play(animationName, 0, 0f);
        }
    }
}