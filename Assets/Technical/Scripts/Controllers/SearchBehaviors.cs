﻿using System;
using Technical.Scripts.Controllers.Spells;
using Technical.Scripts.Controllers.Structures;
using Technical.Scripts.Controllers.Units;
using Technical.Scripts.Controllers.Units.Ballista;
using Technical.Scripts.Controllers.Units.Priest;
using UnityEngine;

namespace Technical.Scripts.Controllers
{
    public abstract class SelectTargetBehavior
    {
        [Serializable]
        public enum Behavior
        {
            None,
            Unit,
            Priest,
            Ballista,
            Structure
        }

        public static SelectTargetBehavior Create(Behavior behavior)
        {
            switch (behavior)
            {
                case Behavior.Unit: return new UnitSelectTargetBehavior();
                case Behavior.Priest: return new PriestSelectTargetBehavior();
                case Behavior.Ballista: return new BallistaSelectTargetBehavior();
                case Behavior.Structure: return new StructureSelectTargetBehavior();
                default: return null;
            }
        }
        
        public abstract GameObject SelectTarget(Collider[] potentialTargets, GameObject currentTarget, GameObject searcher);
        
    }
    
    public abstract class SelectTargetsBehavior
    {
        [Serializable]
        public enum Behavior
        {
            None,
            AoE
        }

        public static SelectTargetsBehavior Create(Behavior behavior)
        {
            switch (behavior)
            {
                case Behavior.AoE: return new AoeSelectTargetsBehavior();
                default: return null;
            }
        }
        public abstract GameObject[] SelectTargets(Collider[] potentialTargets, GameObject searcher);
    }

    public abstract class FindTargetBehavior
    {
        [Serializable]
        public enum Behavior
        {
            None,
            Unit,
            Structure,
            AoE
        }

        public static FindTargetBehavior Create(Behavior behavior)
        {
            switch (behavior)
            {
                case Behavior.Unit: return new UnitFindTargetBehavior();
                case Behavior.Structure: return new StructureFindTargetBehavior();
                case Behavior.AoE: return new AoeFindTargetBehavior();
                default: return null;
            }
        }
        public abstract Collider[] FindNearbyTargets(Vector3 position, float range, LayerMask whatIsTarget);
    }
}