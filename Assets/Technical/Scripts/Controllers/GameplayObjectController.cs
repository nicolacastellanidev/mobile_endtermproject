﻿using System;
using System.Collections.Generic;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Managers;
using Technical.Scripts.Models;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Controllers
{
    [Serializable]
    public enum UnitStatus
    {
        SLEEPING,
        ACTIVE
    }
    
    [Serializable]
    public class GameplayObjectStateEvent : UnityEvent<UnitStatus> {}
    
    [RequireComponent(typeof(Collider))]
    public class GameplayObjectController: MonoBehaviour
    {
        public GameplayObjectStateEvent onGameplayObjectStateChange;
        
        [SerializeField] private UnitStatus unitStatus = UnitStatus.SLEEPING;
        private UnitStatus _internalUnitStatus = UnitStatus.SLEEPING;
        
        [SerializeField] private GameplayObject configuration;

        private Collider gameplayObjectCollider;
        
        private void Start()
        {
            GameplayObjectManager.Instance.RegisterGPO(this, configuration.team);
            gameplayObjectCollider = GetComponent<Collider>();
            ShouldEnableCollisionsForGameplayObject();
        }

        private void FixedUpdate()
        {
            VerifyStatus();
        }

        public void Sleep()
        {
            unitStatus = UnitStatus.SLEEPING;
        }
        
        public void Activate()
        {
            unitStatus = UnitStatus.ACTIVE;
        }

        private void OnDestroy()
        {
            OnDie();
        }

        public void OnDie()
        {
            GameplayObjectManager.Instance.UnregisterGPO(this, configuration.team);
            gameObject.layer = LayerMask.NameToLayer("Dead");
            DisableComponents();
        }

        public GameplayObjectModel GetModel()
        {
            return configuration.model;
        }

        public Team GetTeam()
        {
            return configuration.team;
        }
        
        public GameplayObjectType GetType()
        {
            return configuration.type;
        }

        public bool IsUnit()
        {
            return configuration.type.IsUnit();
        }
        
        public bool IsStructure()
        {
            return configuration.type.IsStructure();
        }

        private void DisableComponents()
        {
            DisableOnGameplayObjectDiedBehaviour[] disableComponentsOnGameplayObjectDied = GetComponents<DisableOnGameplayObjectDiedBehaviour>();
            foreach (var comp in disableComponentsOnGameplayObjectDied)
                Destroy(comp);
        }

        private void VerifyStatus()
        {
            if (unitStatus == _internalUnitStatus) return;
            _internalUnitStatus = unitStatus;
            ShouldEnableCollisionsForGameplayObject();
            onGameplayObjectStateChange?.Invoke(_internalUnitStatus);
        }

        private void ShouldEnableCollisionsForGameplayObject()
        {
            gameplayObjectCollider.enabled = _internalUnitStatus == UnitStatus.ACTIVE;
        }
    }
}