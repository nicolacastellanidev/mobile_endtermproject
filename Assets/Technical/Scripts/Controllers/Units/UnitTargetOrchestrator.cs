﻿using System;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Managers;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Controllers.Units
{
    public class UnitTargetOrchestrator : DisableOnGameplayObjectDiedBehaviour
    {
        private GameObject mCurrentTarget;
        private Vector3? mCurrentDestination;

        [Serializable] public class DestinationUpdateEvent : UnityEvent<Vector3?>
        {
        }

        public DestinationUpdateEvent onDestinationUpdate;

        [Serializable] public class TargetUpdateEvent : UnityEvent<GameObject>
        {
        }

        public TargetUpdateEvent onTargetUpdate;

        private bool _isActive = false;

        private GameplayObjectController mController;

        public void OnStatusChange(UnitStatus status)
        {
            _isActive = status == UnitStatus.ACTIVE;
            onTargetUpdate?.Invoke(_isActive ? mCurrentTarget : null);
            onDestinationUpdate?.Invoke(_isActive ? mCurrentDestination : null);
        }

        protected void Awake()
        {
            mController = GetComponent<GameplayObjectController>();
        }

        private void Update()
        {
            if (!_isActive) return;
            LookAtTarget();
            if (mCurrentDestination != null || mCurrentTarget != null) return;
            SetTarget(
                GameplayObjectManager.Instance
                    .GetDefaultTargetFor(
                        mController)); // default destination behavior should be dependent on gameplayobject
        }

        public void OnTargetInRange(bool inRange)
        {
            if (!_isActive) return;

            if (inRange)
            {
                SetDestination(null);
            }
            else
            {
                SetDestination(mCurrentTarget.transform.position);
            }
        }

        public void SetTarget(GameObject nextTarget)
        {
            if (nextTarget == mCurrentTarget) return;
            mCurrentTarget?.GetComponent<BaseHealthController>().onGameplayObjectDead.RemoveListener(OnTargetDied);
            mCurrentTarget = nextTarget;
            if (!_isActive) return;
            onTargetUpdate?.Invoke(mCurrentTarget);
            if (mCurrentTarget == null)
            {
                SetDestination(null);
                return;
            }
            mCurrentTarget.GetComponent<BaseHealthController>().onGameplayObjectDead.AddListener(OnTargetDied);
        }
        
        private void OnTargetDied(GameObject _target)
        {
            print($"{_target.name} is dead, should get new target.");
            SetTarget(null);
            SetDestination(null);
        }
        
        private void SetDestination(Vector3? destination)
        {
            mCurrentDestination = destination;
            if (_isActive)
                onDestinationUpdate?.Invoke(mCurrentDestination);
        }

        private void LookAtTarget()
        {
            if (!mController.IsUnit()) return;
            if (mCurrentDestination == null && mCurrentTarget == null) return;
            LookAt(mCurrentTarget ? mCurrentTarget.transform.position : (Vector3) mCurrentDestination);
        }

        private void LookAt(Vector3 dest)
        {
            var position = transform.position;
            var lookPos = dest - position;
            lookPos.y = 0;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookPos), .1f);
        }
    }
}