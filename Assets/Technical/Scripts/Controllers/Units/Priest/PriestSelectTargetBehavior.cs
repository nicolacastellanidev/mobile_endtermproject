﻿using System.Linq;
using Technical.Scripts.Controllers.Base;
using UnityEngine;

namespace Technical.Scripts.Controllers.Units.Priest
{
    public class PriestSelectTargetBehavior : SelectTargetBehavior
    {
        public override GameObject SelectTarget(Collider[] potentialTargets, GameObject currentTarget, GameObject searcher)
        {
            if (currentTarget)
            {
                BaseHealthController currentTargetHealthController =
                    currentTarget?.GetComponent<BaseHealthController>();
                if (currentTargetHealthController && !currentTargetHealthController.IsMaxHP())
                    return currentTarget;
            }

            var targets = potentialTargets
                .Where(c =>
                    c != null &&
                    c.gameObject != searcher &&
                    c.GetComponent<GameplayObjectController>().IsUnit())
                .OrderBy(c => (c.transform.position - searcher.transform.position).sqrMagnitude);

            return targets
                .Where(c => !c.gameObject.GetComponent<BaseHealthController>().IsMaxHP())
                .OrderBy(c => c.gameObject.GetComponent<BaseHealthController>().GetHealthRemainingPercentage())
                .FirstOrDefault()
                ?.gameObject;
        }
    }
}