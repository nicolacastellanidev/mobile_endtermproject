﻿using Technical.Scripts.Commands;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Models;
using Technical.Scripts.Utils;
using UnityEngine;

namespace Technical.Scripts.Controllers.Units.Priest
{
    public class PriestAttackController : BaseInteractionController
    {
        [SerializeField] private PriestInteraction interactionData;
        
        protected override void Awake()
        {
            base.Awake();
            interactionCommand = new SingleHealCommand(interactionData.healAmount, interactionData.healEffect);
            SetBaseInteractionData(interactionData.baseInteraction);
        }

        protected override void ExecuteInteraction()
        {
            if (!target || !CanHealTarget()) return;
            animator.Animate(UnitAnimationController.Attack, 1 / interactionData.baseInteraction.ratio);
            audioHandler?.PlayAudioAction(AudioActionType.Attack);
        }

        [CalledByAnimationEvent]
        public void Heal(AnimationEventType eventType)
        {
            if (!target) return;
            if (!target || !CanHealTarget())
            {
                animator.Animate(UnitAnimationController.Idle);
            }
            else
            {
                interactionCommand.Execute(target.transform);
            }
        }

        private bool CanHealTarget()
        {
            var isMaxHp = target.GetComponent<BaseHealthController>().IsMaxHP();
            return !isMaxHp;
        }

        protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, interactionData.baseInteraction.range);
        }
    }
}