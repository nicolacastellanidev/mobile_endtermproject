﻿using System;
using Technical.Scripts.Commands;
using Technical.Scripts.Commands.Base;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Models;
using Technical.Scripts.Utils;
using UnityEngine;

namespace Technical.Scripts.Controllers.Units.Hero
{
    public class HeroAttackController : BaseInteractionController
    {

        [SerializeField] private HeroInteraction interactionData;
        
        private int mCastSpecialAfterAttackCount = 0;
        
        private Command spellCommand;

        protected override void Awake()
        {
            base.Awake();
            var heroMeleeInteraction = interactionData.meleeInteraction;
            interactionCommand = new SingleHitCommand(heroMeleeInteraction.hitAmount, heroMeleeInteraction.hitEffect, heroMeleeInteraction.hitEffectSpawnPoint);
            spellCommand = new SpellCommand(transform, interactionData.specialAttack, interactionData.specialAttackTargetLayer);
            SetBaseInteractionData(interactionData.meleeInteraction.baseInteraction);
        }

        protected override bool IsTargetInRange()
        {
            var _transform = transform;
            return Vector3.Distance(_transform.position, target.transform.position) < interactionData.meleeInteraction.baseInteraction.range;
        }

        protected override void ExecuteInteraction()
        {
            if (!target || !IsTargetInRange()) return;
            
            if (mCastSpecialAfterAttackCount >= interactionData.attackCountForSpecialAttack)
            {
                animator.Animate(UnitAnimationController.SpecialAttack, 1 / interactionData.meleeInteraction.baseInteraction.ratio);
                mCastSpecialAfterAttackCount = 0;
                return;
            }

            mCastSpecialAfterAttackCount++;
            animator.Animate(UnitAnimationController.Attack, 1 / interactionData.meleeInteraction.baseInteraction.ratio);
        }
        
        [CalledByAnimationEvent]
        public void DealDamage(AnimationEventType eventType)
        {
            if (!target || !IsTargetInRange()) return;

            switch (eventType)
            {
                case AnimationEventType.DealDamage:
                    interactionCommand.Execute(target.transform);
                    break;
                case AnimationEventType.SpecialAttack:
                    spellCommand.Execute();
                    audioHandler?.PlayAudioAction(AudioActionType.Engage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(eventType), eventType, null);
            }
        }

        protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, interactionData.meleeInteraction.baseInteraction.range);
        }
    }
}