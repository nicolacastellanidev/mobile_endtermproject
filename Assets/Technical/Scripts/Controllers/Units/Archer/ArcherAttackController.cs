﻿using Technical.Scripts.Commands;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Models;
using Technical.Scripts.Utils;
using UnityEngine;

namespace Technical.Scripts.Controllers.Units.Archer
{
    /// <summary>
    /// Attack controller for Archers unit
    /// </summary>
    public class ArcherAttackController : BaseInteractionController
    {
        [SerializeField] private RangedInteraction interactionData;

        protected override void Awake()
        {
            base.Awake();
            interactionCommand = new SingleRangedAttackCommand(interactionData.bullet, interactionData.bulletSpawnPoint, gameObject);
            SetBaseInteractionData(interactionData.baseInteraction);
        }

        protected override bool IsTargetInRange()
        {
            if(!target) return false;
            var _transform = transform;
            var position = _transform.position + new Vector3(0, 0.5f, 0);
            var direction = (target.transform.position - position).normalized;
            return Physics.Raycast(position, direction, interactionData.baseInteraction.range, 1 << target.layer);
        }

        protected override void ExecuteInteraction()
        {
            animator?.Animate(UnitAnimationController.Attack, 1 / interactionData.baseInteraction.ratio);
        }
        
        [CalledByAnimationEvent]
        public void SpawnProjectile(AnimationEventType type)
        {
            if(!target) return;
            interactionCommand?.Execute(target.transform);
            audioHandler.PlayAudioAction(AudioActionType.Attack);
        }

        protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, interactionData.baseInteraction.range);
        }
    }
}