﻿using Technical.Scripts.Commands;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Models;
using Technical.Scripts.Utils;
using UnityEngine;

namespace Technical.Scripts.Controllers.Units.Melee
{
    public class MeleeAttackController : BaseInteractionController
    {
        [SerializeField] private MeleeInteraction interactionData;
        
        protected override void Awake()
        {
            base.Awake();
            interactionCommand = new SingleHitCommand(interactionData.hitAmount, interactionData.hitEffect, interactionData.hitEffectSpawnPoint);
            SetBaseInteractionData(interactionData.baseInteraction);
        }

        protected override bool IsTargetInRange()
        {
            if (!target) return false;
            var _transform = transform;
            // this fixes some issues
            var position = _transform.position + new Vector3(0, 0.25f, 0);
            var direction = (target.transform.position - position).normalized;
            bool isInRange =  Physics.Raycast(position, direction, interactionData.baseInteraction.range, 1 << target.layer);
            
            return isInRange || Vector3.Distance(transform.position, target.transform.position) < interactionData.baseInteraction.range;

        }

        protected override void ExecuteInteraction()
        {
            if (!target || !IsTargetInRange()) return;
            animator.Animate(UnitAnimationController.Attack, 1 / interactionData.baseInteraction.ratio);
        }

        [CalledByAnimationEvent]
        public void DealDamage(AnimationEventType eventType)
        {
            if (!IsTargetInRange()) return;
            interactionCommand.Execute(target.transform);
            if (audioHandler)
                audioHandler.PlayAudioAction(AudioActionType.Attack);
        }

        protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, interactionData.baseInteraction.range);
        }
    }
}