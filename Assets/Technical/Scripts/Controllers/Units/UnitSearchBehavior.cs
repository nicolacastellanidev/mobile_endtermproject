﻿using System.Linq;
using UnityEngine;

namespace Technical.Scripts.Controllers.Units
{
    public class UnitSelectTargetBehavior : SelectTargetBehavior
    {
        public override GameObject SelectTarget(Collider[] potentialTargets, GameObject currentTarget, GameObject searcher)
        {
            var searcherGameplayObject = searcher.GetComponent<GameplayObjectController>();
            if (!searcherGameplayObject)
            {
                Debug.LogWarning("GameplayObjectController is missing on searcher");
                return null;
            }

            var target = potentialTargets
                .Where(c => c != null && c.gameObject != searcher)
                .OrderBy(c => (c.transform.position - searcher.transform.position).sqrMagnitude)
                .FirstOrDefault()?.gameObject;

            return target;
        }
    }
    
    public class UnitFindTargetBehavior : FindTargetBehavior
    {
        public override Collider[] FindNearbyTargets(Vector3 position, float range, LayerMask whatIsTarget)
        {
            var colliders = new Collider[10];
            Physics.OverlapSphereNonAlloc(position, range, colliders, whatIsTarget);
            return colliders;
        }
    }

}