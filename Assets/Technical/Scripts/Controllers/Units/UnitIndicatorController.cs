﻿using UnityEngine;
using UnityEngine.Animations;

namespace Technical.Scripts.Controllers.Units
{
    [RequireComponent(typeof(LookAtConstraint))]
    public class UnitIndicatorController : MonoBehaviour
    {
        private LookAtConstraint m_lookAt;
        private void Awake()
        {
            m_lookAt = GetComponent<LookAtConstraint>();
        }

        private void Start()
        {
            var source = new ConstraintSource();
            if (!(Camera.main is null)) source.sourceTransform = Camera.main.transform;
            source.weight = 1;
            m_lookAt.AddSource(source);
        }
    }
}
