﻿using Technical.Scripts.Controllers.Base;
using UnityEngine;

namespace Technical.Scripts.Controllers.Units
{
    [RequireComponent(typeof(GameplayObjectController))]
    public class UnitSearchController : BaseSearchController
    {
        protected void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, data.range);
        }
    }
}