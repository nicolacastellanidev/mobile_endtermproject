﻿using Technical.Scripts.Utils;
using UnityEngine;

namespace Technical.Scripts.Controllers.Units.Ballista
{
    public class BallistaAnimationEventHandler : AnimationEventHandler
    {
        [SerializeField] private GameObject bullet;
      
        [CalledByAnimationEvent]
        public void ShowArrow()
        {
            bullet.SetActive(true);
        }
        [CalledByAnimationEvent]
        public void HideArrow()
        {
            bullet.SetActive(false);
        }

    }
}