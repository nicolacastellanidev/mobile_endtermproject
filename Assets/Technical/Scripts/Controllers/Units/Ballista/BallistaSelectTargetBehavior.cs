﻿using System.Linq;
using UnityEngine;

namespace Technical.Scripts.Controllers.Units.Ballista
{
    public class BallistaSelectTargetBehavior : SelectTargetBehavior
    {
        public override GameObject SelectTarget(Collider[] potentialTargets, GameObject currentTarget, GameObject searcher)
        {
            var searcherGameplayObject = searcher.GetComponent<GameplayObjectController>();
            if (!searcherGameplayObject)
            {
                Debug.LogWarning("GameplayObjectController is missing on searcher");
                return null;
            }

            var orderedByPositionPotentialTargets = potentialTargets
                .Where(c => c != null && c.gameObject != searcher)
                .OrderBy(c => (c.transform.position - searcher.transform.position).sqrMagnitude);

            return orderedByPositionPotentialTargets
                       .FirstOrDefault(c => c.gameObject.GetComponent<GameplayObjectController>().IsStructure())
                       ?.gameObject
                   ?? orderedByPositionPotentialTargets.FirstOrDefault()?.gameObject;
        }
    }
}