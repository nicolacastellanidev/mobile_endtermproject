﻿using Technical.Scripts.Commands;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Models;
using Technical.Scripts.Utils;
using UnityEngine;

namespace Technical.Scripts.Controllers.Units.Ballista
{
    public class BallistaAttackController : BaseInteractionController
    {
        [SerializeField] private BallistaInteraction interactionData;

        protected override void Awake()
        {
            base.Awake();
            var rangedInteraction = interactionData.rangedInteraction;
            interactionCommand = new SingleRangedAttackCommand(rangedInteraction.bullet, rangedInteraction.bulletSpawnPoint, gameObject);
            SetBaseInteractionData(interactionData.rangedInteraction.baseInteraction);
        }

        protected override bool IsTargetInRange()
        {
            if(!target) return false;
            var _transform = transform;
            var position = _transform.position + new Vector3(0, 0.5f, 0);
            var direction = (target.transform.position - position).normalized;
            return Physics.Raycast(position, direction, interactionData.rangedInteraction.baseInteraction.range, 1 << target.layer);
        }

        protected override void ExecuteInteraction()
        {
            if (!CanAttackTarget())
            {
                animator.Animate(UnitAnimationController.Idle);
            }
            else
            {
                animator.Animate(UnitAnimationController.Attack, 1 / interactionData.rangedInteraction.baseInteraction.ratio);
            }
        }
        
        [CalledByAnimationEvent]
        public void SpawnProjectile(AnimationEventType type)
        {
            if(!target) return;
            audioHandler?.PlayAudioAction(AudioActionType.Attack);
            interactionCommand?.Execute(target.transform);
        }
        
        protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, interactionData.rangedInteraction.baseInteraction.range);
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(transform.position, interactionData.attackDeniedRange);
        }

        private bool CanAttackTarget()
        {
            if(!target) return false;
            var position = transform.position;
            var direction = (target.transform.position - position).normalized;
            return Physics.Raycast(position, direction, interactionData.attackDeniedRange, 1 << target.layer) == false;
        }
    }
}