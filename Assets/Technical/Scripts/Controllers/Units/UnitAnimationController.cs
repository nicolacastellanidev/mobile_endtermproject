﻿using Technical.Scripts.Controllers.Base;

namespace Technical.Scripts.Controllers.Units
{
    public class UnitAnimationController: BaseAnimationController
    {
        public static string Idle => "Idle";
        public static string Attack => "Attack";
        public static string Move => "Walk";
        public static string Die => "Die";
        public static string SpecialAttack => "SpecialAttack";
    }
}