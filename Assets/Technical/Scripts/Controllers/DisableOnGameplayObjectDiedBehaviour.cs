﻿using UnityEngine;

namespace Technical.Scripts.Controllers
{
    public abstract class DisableOnGameplayObjectDiedBehaviour : MonoBehaviour{ };
}