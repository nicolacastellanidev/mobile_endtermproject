﻿using UnityEngine;

namespace Technical.Scripts.Controllers
{

    public class TrajectoryController : MonoBehaviour
    {
        private float angle = 45f;
        private Rigidbody rb;
        private bool hasLaunched = false;

        private void Awake()
        {
            rb = GetComponentInChildren<Rigidbody>();
        }

        private void Update()
        {
            if(hasLaunched)
                transform.rotation = Quaternion.LookRotation(rb.velocity);
        }

        public void Launch(Vector3 targetPosition)
        {
            //make sure gameobject look at target (except for y axis)
            var selfPosXZ = new Vector3(transform.position.x, 0f, transform.position.z);
            var targetPosXZ = new Vector3(targetPosition.x, 0f, targetPosition.z);
            transform.LookAt(targetPosXZ);
            //compute core elements for projectile motion eq
            var distance = Vector3.Distance(selfPosXZ, targetPosXZ);
            var grav = Physics.gravity.y;
            var tanAlpha = Mathf.Tan(angle * Mathf.Deg2Rad);
            var heightZero = targetPosition.y - transform.position.y;
            //local velocity components
            var Vz = Mathf.Sqrt(grav * distance * distance / (2f * (heightZero - distance * tanAlpha)));
            var Vy = tanAlpha * Vz;
            //convert local velocity to global
            var vel = transform.TransformDirection(new Vector3(0f, Vy, Vz));
            rb.velocity = vel;
            hasLaunched = true;
        }
    }
}