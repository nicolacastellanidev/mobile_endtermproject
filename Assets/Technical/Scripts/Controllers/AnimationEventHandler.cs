﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Controllers
{
    [Serializable]
    public enum AnimationEventType
    {
        DealDamage,
        SpecialAttack
    }
    public class AnimationEventHandler : MonoBehaviour
    {
        [Serializable]
        public class OnAnimationEvent : UnityEvent<AnimationEventType>{}
        public OnAnimationEvent eventTrigger;

        public void DealDamageEvent()
        {
            eventTrigger?.Invoke(AnimationEventType.DealDamage);
        }
        
        public void SpecialAttackEvent()
        {
            eventTrigger?.Invoke(AnimationEventType.SpecialAttack);
        }
    }
}
