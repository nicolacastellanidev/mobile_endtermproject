﻿using System;
using Technical.Scripts.Controllers;
using UnityEngine;

namespace Technical.Scripts.Models
{
    [Serializable]
    public struct PlayerUnitSettingsProperties
    {
        public int cost;
        [SerializeField] private GameplayObjectController prefabPlayer1;
        [SerializeField] private GameplayObjectController prefabPlayer2;

        public GameplayObjectController GetPrefabForTeam(Team team)
        {
            return team == Team.PLAYER ? prefabPlayer1 : prefabPlayer2;
        }
    }

    [CreateAssetMenu(fileName = "GameModeSettings", menuName = "GameMode/UnitSettings", order = 2)]
    [Serializable]
    public class PlayerUnitSettings : ScriptableObject
    {
        // max units for each team
        public PlayerUnitSettingsProperties meleeSettings;
        public PlayerUnitSettingsProperties archerSettings;
        public PlayerUnitSettingsProperties priestSettings;
        public PlayerUnitSettingsProperties heroSettings;
        public PlayerUnitSettingsProperties turretSettings;
        public PlayerUnitSettingsProperties ballistaSettings;
        
        public PlayerUnitSettingsProperties? GetPropertiesByModel(GameplayObjectModel model)
        {
            switch (model)
            {
                case GameplayObjectModel.Melee:
                    return meleeSettings;
                case GameplayObjectModel.Archer:
                    return archerSettings;
                case GameplayObjectModel.Priest:
                    return priestSettings;
                case GameplayObjectModel.Hero:
                    return heroSettings;
                case GameplayObjectModel.Turret:
                    return turretSettings;
                case GameplayObjectModel.Ballista:
                    return ballistaSettings;
                case GameplayObjectModel.AreaOfDamage:
                case GameplayObjectModel.Castle:
                default:
                    return null;
            }
        }
    }
}
