﻿using System;
using UnityEngine;

namespace Technical.Scripts.Models.SimulationMode
{
    [Serializable]
    [CreateAssetMenu(fileName = "IaSpawnable", menuName = "GameMode/Simulation/IaSpawnable", order = 2)]
    public class IaSimulationSpawnable : ScriptableObject
    {
        public GameplayObjectModel model;
        [Range(0, 100)] public int spawnProbability;
        public bool forceSpawn = false;
        public Vector2 spawnPosition;
    }
}
