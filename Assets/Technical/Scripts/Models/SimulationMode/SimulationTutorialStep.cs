﻿using System;
using UnityEngine;

namespace Technical.Scripts.Models.SimulationMode
{
    [CreateAssetMenu(fileName = "SimulationTutorialStep", menuName = "GameMode/TutorialStep", order = 1)]
    [Serializable]
    public class SimulationTutorialStep : ScriptableObject
    {
        [TextArea(3, 10)] public string message;
        public Sprite image;

        public string nextButtonLabel = "Next";
        public string previousButtonLabel = "Previous";
    }
}
