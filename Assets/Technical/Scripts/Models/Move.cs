﻿using System;

namespace Technical.Scripts.Models
{
    [Serializable]
    public class Move
    {
        public float speed = 0f;
        public float deltaDistanceToReachDestination = 1f;
    }
}