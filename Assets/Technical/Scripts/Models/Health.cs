﻿using System;
using UnityEngine;

namespace Technical.Scripts.Models
{
    [Serializable]
    public class Health
    {
        public float life;
        public float dieAfter = 0f;
        public GameObject dieEffect;
    }
}