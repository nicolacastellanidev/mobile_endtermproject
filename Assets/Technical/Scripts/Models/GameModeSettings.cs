﻿using System;
using UnityEngine;

namespace Technical.Scripts.Models
{

    
    [CreateAssetMenu(fileName = "GameModeSettings", menuName = "GameMode/Settings", order = 1)]
    [Serializable]
    public class GameModeSettings : ScriptableObject
    {
        // max units for each team
        public int maxSpawnableUnits = 10;
        public PlayerUnitSettings playerUnitSettings;
        public string startTitle = "Welcome Player!";
        [TextArea(3, 10)] public string startText = "It's time for battle! Click on the blue cells and spawn units over them.";
        public string levelId = "";
    }
}
