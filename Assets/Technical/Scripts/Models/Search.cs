﻿using System;
using Technical.Scripts.Controllers;
using UnityEngine;

namespace Technical.Scripts.Models
{
    [Serializable]
    public class Search
    {
        public float range = 0f;
        public LayerMask whatIsTarget = 0;
        public float refreshRate = .1f;
        public SelectTargetBehavior.Behavior selectTargetBehavior;
        public FindTargetBehavior.Behavior findTargetBehavior;
    }
    
    [Serializable]
    public class SpellSearch
    {
        public float range = 0f;
        public LayerMask whatIsTarget = 0;
        public float refreshRate = .1f;
        public SelectTargetsBehavior.Behavior selectTargetsBehavior;
        public FindTargetBehavior.Behavior findTargetBehavior;
    }
}