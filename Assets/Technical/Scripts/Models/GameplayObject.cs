﻿using System;
using UnityEngine;

namespace Technical.Scripts.Models
{
    public enum GameplayObjectType
    {
        Structure,
        Unit,
        Spell
    }

    [Serializable]
    public enum GameplayObjectModel
    {
        //Structures
        Castle,
        Turret,
        
        //Unit
        Melee,
        Archer,
        Priest,
        Hero,
        Ballista,
        // Spell
        AreaOfDamage
    }

    internal static class GameplayObjectHelper
    {
        private static GameplayObjectModel[] StructureModels = new[]
            {GameplayObjectModel.Castle, GameplayObjectModel.Turret};
        private static GameplayObjectModel[] UnitModels = new[]
            {GameplayObjectModel.Melee, GameplayObjectModel.Hero, GameplayObjectModel.Archer, GameplayObjectModel.Priest, GameplayObjectModel.Ballista};
        
        public static GameplayObjectModel[] GetStructureModels()
        {
            return StructureModels;
        }
        
        public static GameplayObjectModel[] GetUnitModels()
        {
            return UnitModels;
        }

        public static bool IsUnit(this GameplayObjectType type)
        {
            return type == GameplayObjectType.Unit;
        }
        
        public static bool IsStructure(this GameplayObjectType type)
        {
            return type == GameplayObjectType.Structure;
        }
    }
    
    [Serializable]
    public class GameplayObject
    {
        public Team team;
        public GameplayObjectType type;
        public GameplayObjectModel model;
    }
}