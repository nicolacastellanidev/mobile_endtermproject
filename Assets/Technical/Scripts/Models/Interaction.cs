﻿using System;
using Technical.Scripts.Commands.Spells;
using Technical.Scripts.Spells;
using UnityEngine;

namespace Technical.Scripts.Models
{
    [Serializable]
    public class Interaction
    {
        [Range(0.1f, 10f)] public float ratio = 1f;
        [Range(0.1f, 30f)] public float range = 1f;
    }

    [Serializable]
    public class PriestInteraction
    {
        public Interaction baseInteraction;
        public float healAmount = 0f;
        public GameObject healEffect;
    }
    
    [Serializable]
    public class RangedInteraction
    {
        public Interaction baseInteraction;
        public GameObject bullet;
        public Transform bulletSpawnPoint;
    }

    [Serializable]
    public class MeleeInteraction
    {
        public Interaction baseInteraction;
        public float hitAmount = 0f;
        public GameObject hitEffect;
        public Transform hitEffectSpawnPoint;
    }
    
    [Serializable]
    public class HeroInteraction
    {
        public MeleeInteraction meleeInteraction;
        public GameObject specialAttack;
        public LayerMask specialAttackTargetLayer;
        public int attackCountForSpecialAttack = 5;
    }
    
    [Serializable]
    public class BallistaInteraction
    {
        public RangedInteraction rangedInteraction;
        [Range(1f, 20f)] public float attackDeniedRange = 5f; 
    }
    
    [Serializable]
    public class SpellInteraction
    {
        public float amount;
        public Spell spellToExecute;
    }
}