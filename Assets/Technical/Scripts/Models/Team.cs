﻿using System;

namespace Technical.Scripts.Models
{
    [Serializable]
    public enum Team
    {
        PLAYER,
        AI
    }

    public static class TeamExtension
    {
        public static Team getEnemyTeam(this Team team)
        {
            return team == Team.PLAYER ? Team.AI : Team.PLAYER;
        }
    }
}