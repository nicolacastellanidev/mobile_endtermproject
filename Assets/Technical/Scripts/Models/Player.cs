﻿using System;
using System.Collections.Generic;
using System.Linq;
using Technical.Scripts.Controllers;
using UnityEngine;

namespace Technical.Scripts.Models
{
    [Serializable]
    public class Player
    {
        private Team mTeam;
        public Team Team => mTeam;

        public int unitOccupation = 0;
        
        private readonly List<GameplayObjectController> playerGPOs = new List<GameplayObjectController>();
        private Vector3 currentDestination;

        public Player(Team team)
        {
            mTeam = team;
        }

        public void RegisterGPO(GameplayObjectController gpo)
        {
            playerGPOs.Add(gpo);
        }
        
        public void UnregisterGPO(GameplayObjectController gpo)
        {
            playerGPOs.Remove(gpo);
        }

        public List<GameplayObjectController> GetAllGPO()
        {
            return playerGPOs;
        }
        
        public List<GameObject> GetAllGPOGameObject()
        {
            return playerGPOs.Select(gameplayObjectController => gameplayObjectController.gameObject).ToList();
        }
    }
}