﻿using System;
using System.Linq;
using JetBrains.Annotations;

namespace Technical.Scripts.Models
{
    [Serializable]
    public class PersistentData
    {
        public GameSettings settings;
        public GameData gameData;

        public PersistentData()
        {
            settings = new GameSettings();
            gameData = new GameData();
        }
    }

    [Serializable]
    public class GameSettings
    {
        public CameraSettings cameraSettings;
        public VolumeSettings volumeSettings;

        public GameSettings()
        {
            cameraSettings = new CameraSettings();
            volumeSettings = new VolumeSettings();
        }
    }
    
    [Serializable]
    public class CameraSettings
    {
        public float cameraZoomSpeed = 50;
        public float maxCameraZoom = 5;
        public float minCameraZoom = 20;

        public float cameraRotationSpeed = 50;
    }
    
    [Serializable]
    public class VolumeSettings
    {
        public float masterVolume = maxVolume;
        public float musicVolume = -10;
        public float uiVolume = -10;
        public float sfxVolume = -10;
        
        public static float minVolume = -80f;
        public static float maxVolume = 0f;
    }

    [Serializable]
    public class GameData
    {
        public LevelData[] levels;

        public GameData()
        {
            levels = new LevelData[6];
            for (var i = 0; i < 6; i++)
            {
                levels[i] = new LevelData();
                
                if (i < 5)
                {
                    levels[i].hasNextLevel = true;
                    levels[i].id = $"1-{i + 1}";
                    levels[i].nextLevelId = $"1-{i + 2}";
                }
                else
                {
                    levels[i - 1].nextLevelId = "ARENA";
                    levels[i].hasNextLevel = false;
                    levels[i].id = "ARENA";
                }
            }
            levels[0].accessible = true;
        }
        
        public void LevelComplete(string levelId, int score)
        {
            for (var i = 0; i < levels.Length; i++)
            {
                if (levels[i].id != levelId) continue;
                if (score <= levels[i].score) return;
                
                levels[i].score = score;

                if (i + 1 < levels.Length)
                {
                    levels[i + 1].accessible = true;
                }

                return;
            }
        }

        [CanBeNull]
        public LevelData GetLevel(string levelId)
        {
            return levels.FirstOrDefault(level => level.id == levelId);
        }
    }
    
    [Serializable]
    public class LevelData
    {
        public string id;
        public string nextLevelId;
        public int score = 0;
        public bool accessible = false;
        public bool hasNextLevel = true;
    }
}