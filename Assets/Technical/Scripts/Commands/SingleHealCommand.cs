﻿using Technical.Scripts.Commands.Base;
using Technical.Scripts.Controllers.Base;
using UnityEngine;

namespace Technical.Scripts.Commands
{
    public sealed class SingleHealCommand: SingleTargetCommand
    {
        private readonly GameObject mHealEffectPrefab;

        public override void Execute(Transform target)
        {
            if (!target) return;
            target.GetComponent<BaseHealthController>()?.Heal(mAmount);
            Object.Instantiate(mHealEffectPrefab, target.position, target.rotation);
        }

        public SingleHealCommand(float amount, GameObject healEffectPrefab) : base(amount)
        {
            mHealEffectPrefab = healEffectPrefab;
        }
    }
}