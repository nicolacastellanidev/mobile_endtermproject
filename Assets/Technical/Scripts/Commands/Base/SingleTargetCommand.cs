﻿using UnityEngine;

namespace Technical.Scripts.Commands.Base
{
    public abstract class SingleTargetCommand
    {
        protected float mAmount;

        protected SingleTargetCommand(float amount)
        {
            mAmount = amount;
        }

        public abstract void Execute(Transform target);
    }
}