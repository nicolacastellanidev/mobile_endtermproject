﻿namespace Technical.Scripts.Commands.Base
{
    public abstract class Command
    {
        public abstract void Execute();
    }
}