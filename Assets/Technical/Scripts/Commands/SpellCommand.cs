﻿using Technical.Scripts.Commands.Base;
using Technical.Scripts.Controllers.Spells;
using UnityEngine;

namespace Technical.Scripts.Commands
{
    public sealed class SpellCommand: Command
    {
        private readonly Transform mParent;
        private readonly GameObject mSpellPrefab;
        private readonly LayerMask mTargetLayer;

        public SpellCommand(Transform parent, GameObject spellPrefab, LayerMask targetLayer)
        {
            mSpellPrefab = spellPrefab;
            mTargetLayer = targetLayer;
            mParent = parent;
        }

        public override void Execute()
        {
            var go = Object.Instantiate(mSpellPrefab, mParent.position, mParent.rotation);
            go.GetComponent<AoeSpellSearchController>()?.SetTarget(mTargetLayer);
        }
    }
}