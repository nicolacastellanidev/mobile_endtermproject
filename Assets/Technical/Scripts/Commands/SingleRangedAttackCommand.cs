﻿using Technical.Scripts.Commands.Base;
using Technical.Scripts.Controllers.Base;
using Technical.Scripts.Controllers.Projectile;
using UnityEngine;
using UnityEngine.AI;

namespace Technical.Scripts.Commands
{
    public sealed class SingleRangedAttackCommand: SingleTargetCommand
    {
        private readonly GameObject mProjectilePrefab;
        private readonly Transform mSpawnPosition;
        private readonly GameObject mParent;
        
        public SingleRangedAttackCommand(GameObject projectilePrefab, Transform spawnPosition, GameObject parent) : base(0)
        {
            mProjectilePrefab = projectilePrefab;
            mSpawnPosition = spawnPosition;
            mParent = parent;
        }
        
        public override void Execute(Transform target)
        {
            if (!target) return;
            var targetPosition = target.position;
            var agent = target.gameObject.GetComponent<NavMeshAgent>();
            if (agent)
            {
                targetPosition += agent.velocity / (2f + Random.Range(-1f, 1f));
            }
            targetPosition.y += 1f; // adds delta to prevent wrong trajectory

            var go = Object.Instantiate(mProjectilePrefab, mSpawnPosition.position, mProjectilePrefab.transform.rotation);
            go
                .GetComponent<BaseProjectileController>()
                .Init(targetPosition, mParent, target.gameObject.layer);
        }
    }
}