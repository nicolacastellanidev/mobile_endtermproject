﻿using Technical.Scripts.Commands.Base;
using Technical.Scripts.Controllers.Base;
using UnityEngine;

namespace Technical.Scripts.Commands
{
    public sealed class SingleHitCommand: SingleTargetCommand
    {
        private readonly GameObject mHitEffectPrefab;
        private readonly Transform mHitEffectSpawnPoint;

        public SingleHitCommand(float amount, GameObject hitEffectPrefab, Transform hitEffectSpawnPoint) : base(amount)
        {
            mHitEffectPrefab = hitEffectPrefab;
            mHitEffectSpawnPoint = hitEffectSpawnPoint;
        }
        
        public override void Execute(Transform target)
        {
            if (!target) return;
            target.GetComponent<BaseHealthController>()?.Hit(mAmount);
            Object.Instantiate(mHitEffectPrefab, mHitEffectSpawnPoint.position, mHitEffectSpawnPoint.rotation);
        }
    }
}