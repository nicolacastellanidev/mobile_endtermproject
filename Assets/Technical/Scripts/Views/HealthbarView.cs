﻿using System;
using UnityEngine;
using UnityEngine.Animations;

namespace Technical.Scripts.Views
{
    /// <summary>
    /// View controller for Healthbar
    /// </summary>
    [Serializable]
    [RequireComponent(typeof(LookAtConstraint))]
    public class HealthbarView : MonoBehaviour

    {
    [SerializeField] private Transform healthbar;
    private GameObject healthbarParent;

    private float maxHealth = 0f;
    private float currentHealth = 0f;
    private new SpriteRenderer renderer;

    private Vector3 targetScale = Vector3.one;
    private Camera mainCamera;
    private LookAtConstraint m_lookAt;

    private void Awake()
    {
        renderer = healthbar.GetComponent<SpriteRenderer>();
        mainCamera = Camera.main;
        targetScale = healthbar.localScale;
        renderer.color = Color.green;
        healthbarParent = healthbar.parent.gameObject;
        healthbarParent.SetActive(false);
        m_lookAt = GetComponent<LookAtConstraint>();
    }

    private void Start()
    {
        var source = new ConstraintSource {sourceTransform = mainCamera.transform, weight = 1};
        m_lookAt.AddSource(source);
    }

    private void LateUpdate()
    {
        healthbar.localScale = Vector3.Lerp(healthbar.localScale, targetScale, 0.1f);
    }

    private void Update()
    {
        Render();
    }

    public void UpdateHealth(float remainingHealth)
    {
        if (maxHealth <= 0f)
            maxHealth = remainingHealth;
        currentHealth = remainingHealth;
        Render();
    }

    private void Render()
    {
        var localScale = healthbar.localScale;
        var percent = Mathf.Max(currentHealth / maxHealth, 0);
        targetScale = new Vector3(percent, localScale.y, localScale.z);
        UpdateColor(percent);
    }

    private void UpdateColor(float percent)
    {
        if (!renderer) return;
        healthbarParent.SetActive(percent < 1);
        var updatedColor = Color.green;
        switch (percent)
        {
            case float n when (n >= 0.75):
                updatedColor = Color.green;
                break;
            case float n when (n > 0.25 && n < 0.75):
                updatedColor = Color.yellow;
                break;
            case float n when (n <= 0.25):
                updatedColor = Color.red;
                break;
        }

        renderer.color = updatedColor;
    }
    }
}