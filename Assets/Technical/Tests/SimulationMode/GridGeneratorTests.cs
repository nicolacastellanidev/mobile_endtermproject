﻿using NUnit.Framework;
using Technical.Scripts.Utils;
using UnityEngine;

namespace Technical.Tests.SimulationMode
{
    public class GridGeneratorTests
    {
        [Test]
        public void ShouldGenerateGrid()
        {
            var grid = GridGenerator.GenerateGrid(5, 10, new GameObject());
            Assert.AreEqual(grid.GetLength(0), 5);
            Assert.AreEqual(grid.GetLength(1), 10);
        }
        
        [Test]
        public void ShouldPlaceGameobjectsCorrectly()
        {
            var grid = GridGenerator.GenerateGrid(5, 10, new GameObject());
            var startX = -5;
            for (var row = 0; row < 5; row++)
            {
                for (var col = 0; col < 10; col++)
                {
                    Assert.AreEqual(grid[row,col].transform.position, new Vector3(startX + col, 0, row));
                }
            } 
        }
        
        [Test]
        public void ShouldPlaceGameobjectsCorrectly_WithInverseDirection()
        {
            var grid = GridGenerator.GenerateGrid(5, 10, new GameObject(), -1);
            var startX = -5;
            for (var row = 0; row < 5; row++)
            {
                for (var col = 0; col < 10; col++)
                {
                    Assert.AreEqual(grid[row,col].transform.position, new Vector3(startX + col, 0, -row));
                }
            } 
        }
        
        [Test]
        public void ShouldPlaceGameobjectsCorrectly_WithGap()
        {
            var gridUpper = GridGenerator.GenerateGrid(5, 10, new GameObject(), 1, 5f);
            var gridLower = GridGenerator.GenerateGrid(5, 10, new GameObject(), -1, 5f);
            var startX = -5;
            for (var row = 0; row < 5; row++)
            {
                for (var col = 0; col < 10; col++)
                {
                    Assert.AreEqual(gridUpper[row,col].transform.position, new Vector3(startX + col, 0, 5f + row));
                }
            }
            
            for (var row = 0; row < 5; row++)
            {
                for (var col = 0; col < 10; col++)
                {
                    Assert.AreEqual(gridLower[row,col].transform.position, new Vector3(startX + col, 0, -5f - row));
                }
            }
        }
        
        [Test]
        public void ShouldPlaceGameobjectsCorrectly_WithCustomCellScale()
        {
            var prefab = new GameObject();
            prefab.transform.localScale = new Vector3(2f, 1f, 2f);
            var startX = -10;
            var gridUpper = GridGenerator.GenerateGrid(5, 10, prefab);

            for (var row = 0; row < 5; row++)
            {
                for (var col = 0; col < 10; col++)
                {
                    Assert.AreEqual(gridUpper[row,col].transform.position, new Vector3(startX + 2f * col, 0, 2f * row));
                }
            }
        }
        
        [Test]
        public void ShouldRotateGameobjectsCorrectly()
        {
            int isGrowingLeft = -1;
            Quaternion isFacingRight = Quaternion.identity;
            int isGrowingRight = 1;
            Quaternion isFacingLeft = Quaternion.Euler(0f, 180f, 0f);

            var gridLeft = GridGenerator.GenerateGrid(5, 10, new GameObject(), isGrowingLeft, 5f);
            var gridRight = GridGenerator.GenerateGrid(5, 10, new GameObject(), isGrowingRight, 5f);
            var startX = -5;

            for (var row = 0; row < 5; row++)
            {
                for (var col = 0; col < 10; col++)
                {
                    Assert.AreEqual(isFacingRight,gridLeft[row,col].transform.rotation);
                    Assert.AreEqual(isFacingLeft, gridRight[row,col].transform.rotation);
                }
            }
        }
    }
}